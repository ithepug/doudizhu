#include "card.h"
#include "mainwindow.h"

#include<QPainter>
#include<QDebug>

//Initialization of static variable
QImage Card::kcard_pic_=QImage(":/image/res/image/card.png");
int Card::kcard_pic_height=160;
int Card::kcard_pic_width=116;

Card::Card(QWidget *parent, CardPoint card_point, CardSuit card_suit, int pair) :
                                                                                  QWidget(parent)
{
  mainwindow_=dynamic_cast<MainWindow*>(parent);
  card_point_=card_point;
  card_suit_=card_suit;
  if(card_point>12)//For joker card.
    card_id_=104+2*(card_point_-13)+pair;
  else
    card_id_=card_point_*8+card_suit_+4*pair;
  card_pic_=QPixmap::fromImage(kcard_pic_).copy(kcard_pic_width*card_point_,kcard_pic_height*card_suit_,kcard_pic_width,kcard_pic_height);
}

Card::Card(QWidget *parent,int card_id):
                                           QWidget (parent)
{
  mainwindow_=dynamic_cast<MainWindow*>(parent);
  card_point_=static_cast<CardPoint>(static_cast<int>(card_id/4));
  card_suit_=static_cast<CardSuit>(card_id%4);
  card_pic_=QPixmap::fromImage(kcard_pic_).copy(kcard_pic_width*card_point_,kcard_pic_height*card_suit_,kcard_pic_width,kcard_pic_height);
}

Card::~Card()
{

}

void Card::paintEvent(QPaintEvent*)
{
  QPainter p(this);
  p.setRenderHint(QPainter::SmoothPixmapTransform);
  p.drawPixmap(this->rect(),card_pic_);
}

int Card::card_point() const
{
  return card_point_;
}

int Card::card_suit() const
{
  return card_suit_;
}

int Card::card_id() const
{
  return card_id_;
}

//Only card is in the selection/selected area, this function do
//somethings.
void Card::mouseReleaseEvent(QMouseEvent *event)
{
  int size;
  if(event->x()>0)
    size=event->x()/35+1;
  else
    size=event->x()/35-2;
  mainwindow_->SelectCard(this,size);
}
