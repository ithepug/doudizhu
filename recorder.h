#ifndef RECORDER_H
#define RECORDER_H

#include <QMessageBox>
#include <QObject>
#include <QUuid>
#include <QtSql>

#include "player.h"
#include "localplayer.h"
#include "onlineplayer.h"

struct EasyPlayer {
  QByteArray uuid_;
  QString name_;
  EasyPlayer(QByteArray uuid, QString name) : uuid_(uuid), name_(name) {}
};

class Recorder : public QObject {
  Q_OBJECT
public:
  static Recorder* GetInstance();

private:
  explicit Recorder(QObject* parent = nullptr);

public:
  bool isReady();

  bool AddLocalPlayer(const LocalPlayer& player);
  bool AddOnlinePlayer(const OnlinePlayer& player);
  LocalPlayer* GetLocalPlayerByUuid(const QByteArray& uuid_byte);
  QList<EasyPlayer> GetLocalPlayers();
  bool UpdatePlayerName(const QByteArray& uuid, const QString& name);
  bool UpdatePlayerPhoto(const QByteArray& uuid, const QByteArray& photo);

  int AddMatch(const QByteArray& uuid);
  int GetMatchIDByMatchUUID(const QByteArray& uuid);
  int GetPlayerMatchScore(const int& match_id, const QByteArray& player_id);
  bool UpdateMatchResult(const int& match_id, const QByteArray& player_id,
                         const int& player_score);

private:
  QSqlDatabase db_;
  bool is_ready_;
};

#endif  // RECORDER_H
