#include "doudizhu.h"

Doudizhu::Doudizhu(QByteArray localplayer_uuid, QString server_ip)
    : QObject(nullptr) {
  playcard_ = new PlayCard(nullptr);
  local_player_ = recorder_->GetLocalPlayerByUuid(localplayer_uuid);
  if (local_player_ == nullptr) {
    QMessageBox::critical(nullptr, "开启失败", "指定用户角色读取失败！");
  }
  transceiver_ = Transceiver::GetInstance();  // CREATE!
  blank_player_ = new Player(QUuid(), "", -1);
  shuffle_interval_time = new QTimer(this);
  score_base_ = 100;

  // Get recorder
  recorder_ = Recorder::GetInstance();
  if (!recorder_->isReady()) {
    QMessageBox::critical(
        nullptr, "开启失败",
        "数据库初始化失败,未能成功开启游戏,请考虑更换游戏存放位置");
  }

  if (server_ip.isEmpty())  // Server mode
  {
    amiserver_ = true;
    transceiver_->SetMode(Transceiver::MODE_SERVER);

    connect(transceiver_, SIGNAL(YouNeedToStopBid()), this, SLOT(StopBid()));

    connect(this, SIGNAL(YouNeedToShuffleCard()), this, SLOT(ShuffleCard()));
    connect(this, SIGNAL(YouNeedToSetLandlord(int)), this,
            SLOT(SetLandlord(int)));
  } else  // Client mode
  {
    amiserver_ = false;
    transceiver_->SetMode(Transceiver::MODE_CLIENT);
    transceiver_->ConnectToHost(server_ip, Transceiver::kPort_);
    // Set online player(Client)
    for (int i = 0; i != 3; i++) {
      OnlinePlayer* new_onlineplayer = new OnlinePlayer;
      online_player_.append(new_onlineplayer);
    }
  }

  connect(shuffle_interval_time, SIGNAL(timeout()), this,
          SLOT(ShuffleOneCard()));
}

Doudizhu* Doudizhu::GetInstance(QByteArray localplayer_uuid,
                                QString server_ip) {
  static Doudizhu* unique_instance = new Doudizhu(localplayer_uuid, server_ip);
  return unique_instance;
}

void Doudizhu::InitMainWindow(MainWindow* mainwindow) {
  mainwindow_ = mainwindow;

  mainwindow_->show();
  InitCard();

  if (amiserver_) {  // Server mode
    mainwindow_->DrawPlayer(*local_player_);
    connect(this, SIGNAL(YouNeedToShowBidBtn(int)), mainwindow_,
            SLOT(ShowBidButton(int)));
  } else {  // client mode
    connect(this, SIGNAL(YouNeedToUpdateLabel()), mainwindow_,
            SLOT(UpdateLabel()));
  }

  mainwindow_->SetPlayerProfile(0, local_player_->photo());

  mainwindow_->UpdateScoreBaseLabel(score_base_);
  connect(mainwindow_->btn_bid_, SIGNAL(clicked()), this,
          SLOT(btn_bid_clicked()));
  connect(mainwindow_->btn_no_bid_, SIGNAL(clicked()), this,
          SLOT(btn_no_bid_clicked()));
  connect(mainwindow_->btn_play_, SIGNAL(clicked()), this,
          SLOT(btn_play_clicked()));
  connect(mainwindow_->btn_pass_, SIGNAL(clicked()), this,
          SLOT(btn_pass_clicked()));
  // Draw card
  connect(local_player_, SIGNAL(YouNeedToDrawCard(int)), mainwindow_,
          SLOT(DrawCard(int)));
  connect(this, SIGNAL(YouNeedToDrawCard(int)), mainwindow_,
          SLOT(DrawCard(int)));
  connect(transceiver_, SIGNAL(YouNeedToDrawCard(int)), mainwindow_,
          SLOT(DrawCard(int)));
}

void Doudizhu::online_player_add(OnlinePlayer *player)
{
  if(player != nullptr) {
    online_player_.append(player);
  }
}

int Doudizhu::online_player_size() const
{
  return online_player_.size();
}

bool Doudizhu::online_player_erase(QTcpSocket *socket)
{
  auto it = online_player_.begin();
  for (; it != online_player_.end(); it++) {
    if ((*it)->tcpsocket() == socket) {
      int i = online_player_.indexOf(*it);
      mainwindow_->ClearLabel(i);
      delete *it;
      online_player_.erase(it);
      break;
    }
  }
  mainwindow_->UpdateLabel();
  transceiver_->TellClinetUpdateInfo(*local_player_, online_player_);
  return true;
}

QTcpSocket *Doudizhu::online_player_tcpsocket(const int &index) const
{
  if(index >= 0 && index < online_player_.size()) {
    return online_player_[index]->tcpsocket();
  } else {
    qDebug() << "Doudizhu::online_player_tcpsocket()"
             << "index out of range";
  }
}

int Doudizhu::online_player_card_size(const int &index) const
{
  if(index >= 0 && index < online_player_.size()) {
    return online_player_[index]->cards_size();
  } else {
    qDebug() << "Doudizhu::online_player_tcpsocket()"
             << "index out of range";
  }
}

QByteArray Doudizhu::local_player_photo() const
{
  return local_player_->photo();
}

int Doudizhu::local_player_card_size() const
{
  return local_player_->cards_size();
}

QList<Card *> Doudizhu::origin_card_list() const
{
  return origin_card_list_;
}

int Doudizhu::current_player() const { return current_player_; }

void Doudizhu::TurnCurrentPlayer() {
  current_player_++;
  current_player_ %= 4;
}

void Doudizhu::set_current_player(const int& player) {
  current_player_ = player;
}

void Doudizhu::set_flag_player_bided(const bool& flag) {
  flag_player_bided_ = flag;
}

void Doudizhu::set_player_is_bid(const bool& is_bid) {
  player_is_bid_ = is_bid;
}

void Doudizhu::set_landlord(Player* player) { landlord_ = player; }

Player* Doudizhu::blank_player() const { return blank_player_; }

int Doudizhu::score_base() const { return score_base_; }

void Doudizhu::AddCardToPlay(Card* card) {
  card_to_play_list_.append(card);
  std::sort(card_to_play_list_.begin(), card_to_play_list_.end(), CardCmp);
}

void Doudizhu::DelCardToPlay(Card* card) {
  auto pos = card_to_play_list_.begin() + card_to_play_list_.indexOf(card);
  card_to_play_list_.erase(pos);
}

bool Doudizhu::CardCmp(const Card* card1, const Card* card2) {
  return card1->card_id() > card2->card_id();
}

void Doudizhu::onReceivedPlayerInfoFromClient(const OnlinePlayer &player)
{
  OnlinePlayer *online_player_p = FindPlayerBySocket(player.tcpsocket());
  if(online_player_p != nullptr) {
    online_player_p->set_uuid(player.uuid());
    online_player_p->set_name(player.name());
    online_player_p->set_photo(player.photo());

    // Draw online player name and score
    mainwindow_->DrawPlayer(*online_player_p);
    // Draw online player photo
    int rank = FindOnlinePlayerRank(online_player_p);
    if(rank != -1) {
      mainwindow_->SetPlayerProfile(rank + 1, online_player_p->photo());
    }

    // Tell other online player
    transceiver_->TellClinetUpdateInfo(*local_player_,online_player_);
    transceiver_->TellAllClientProfile(rank, online_player_[rank]->photo());
    QList<QByteArray> all_online_player_photo;
    for(auto player: online_player_) {
      all_online_player_photo.append(player->photo());
    }
    transceiver_->TellClientAllProfile(rank, all_online_player_photo);

    // Save
    if (!Recorder::GetInstance()->AddOnlinePlayer(*online_player_p)) {
      qDebug() << "OnNewConnection():"
               << "online player保存到数据库失败";
    }

    // If online player size == 3, start game!
    if(online_player_.size() == 3) {
      StartGame();
    }
  }
}

void Doudizhu::onReceivedBidResultFromClient(const bool &isBid)
{
  bid_landlord_mutex_.lock();
  set_flag_player_bided(true);
  set_player_is_bid(isBid);
  bid_landlord_mutex_.unlock();
}

void Doudizhu::onReceivedPlayedCardFromClient(const QList<Card *> &cards)
{
  //Update cards size on player hand.
  online_player_[current_player_ - 1]->SubCardsSize(cards.size());
  emit YouNeedToDrawCard(current_player_ - 1);
  UpdatePlayingCard(cards);

  transceiver_->TellClientPlayedCard(cards);
  transceiver_->TellClientUpdateCardsSize();

  if(online_player_[current_player_ - 1]->cards_size() == 0) {
    CountOut();
  } else {  //Next Player
    TurnCurrentPlayer();
    transceiver_->TellPlayerToPlayCard(current_player_);
  }
}

void Doudizhu::onReceivedPlayerInfoFromServer(const int &index, const OnlinePlayer &player)
{
  if(index >= 0 && index < online_player_.size()){
    online_player_[index]->set_name(player.name());
    online_player_[index]->set_uuid(player.uuid());
    online_player_[index]->set_money(player.money());
  }

  // Save
  if(!player.uuid().isNull())
    Recorder::GetInstance()->AddOnlinePlayer(player);

  // Draw player info (connect to MainWindow)
  emit YouNeedToUpdateLabel();
}

void Doudizhu::onReceivedDealingCardFromServer(const int &card_index)
{
  if(card_index >= 0 && card_index < origin_card_list_.size()) {
    local_player_->AddCard(origin_card_list_[card_index]);

    //Draw online player cards(Clinet).
    for (int i=0;i!=online_player_.size();i++)
    {
      online_player_[i]->AddCardsSize();
      emit YouNeedToDrawCard(i);
    }
  }
}

void Doudizhu::onReceivedClearCardSignalFromServer()
{
  local_player_->ClearCard();
  //Clear online player cards.
  for(int i=0;i!=online_player_.size();i++)
    online_player_[i]->ZeroingCardsSize();
}

void Doudizhu::onReceivedToBidSignalFromServer()
{
  mainwindow_->ShowBidButton(0);
}

void Doudizhu::onReceivedPlayerCardSizeFromServer(const int &index, const int &size)
{
  if(index >= 0 && index < online_player_.size()) {
    online_player_[index]->SetCardsSize(size);
    emit YouNeedToDrawCard(index);
  }
}

void Doudizhu::onReceivedStopBidSignalFromServer()
{
  mainwindow_->HideBidButton();
}

void Doudizhu::onReceivedToPlaySignalFromServer()
{
  mainwindow_->ShowPlayButton();
}

void Doudizhu::onReceivedPlayedCardFromServer(const QList<Card *> &cards)
{
  UpdatePlayingCard(cards);
  if(current_player_ == 0) {
    if(local_player_->cards_size() == 0) {
      CountOut();
    }
  } else if (online_player_[current_player_ - 1]->cards_size()
             == cards.size()) {
    CountOut();
  }
  TurnCurrentPlayer();
}

void Doudizhu::onReceivedCurrentPlayerFromServer(const int &index)
{
  if(index >= 0 && index < 4) {
    set_current_player(index);
  } else {
    qDebug() <<"Doudizhu::onReceivedCurrentPlayerFromServer(): "
             << "index out of range";
  }
}

void Doudizhu::onReceivedClearPlayingCardSignalFromServer()
{
  mainwindow_->ClearAllPlayingCard();
}

void Doudizhu::onReceivedLoadMatchSignalFromServer()
{
  if (!LoadsMatch()) {
    QMessageBox::critical(nullptr, "开启失败", "对局获取/创建失败");
  }
  if (!LoadsPlayersMatchScore()) {
    QMessageBox::critical(nullptr, "开启失败", "用户对局信息获取/创建失败");
  }
}

void Doudizhu::onReceivedLandLordFromServer(const int &index)
{
  if(index >= 0 && index < online_player_.size() + 1) {
    Player* player = index == 0 ? dynamic_cast<Player*>(local_player_)
                                : online_player_[index - 1];
    set_landlord(player);
  } else {
    qDebug() << "Doudizhu::onReceivedLandLordFromServer()"
             << "index out of range";
  }
}

void Doudizhu::onReceivedPlayerPhotoFromServer(const int &index, const QByteArray &photo)
{
  if(index >= 0 && index < online_player_.size()) {
    online_player_[index]->set_photo(photo);
    mainwindow_->SetPlayerProfile(index + 1, online_player_[index]->photo());
  }
}

void Doudizhu::OnConnectedToServer()
{
  Player player;
  player.set_name(local_player_->name());
  player.set_uuid(local_player_->uuid());
  player.set_photo(local_player_->photo());
  transceiver_->TellServerLocalPlayerInfo(player);
}

// Create cards from big to small according to card id .
// To facilitate the overlay display of cards.
// RedJoker and BlackJoker only one suit (defualt:KSpade).
void Doudizhu::InitCard() {
  // RedJoker
  for (int pair = 1; pair >= 0; pair--) {
    Card* redjoker = new Card(mainwindow_, RJ, kSpade, pair);
    redjoker->hide();
    origin_card_list_.append(redjoker);
  }
  // BlackJoker
  for (int pair = 1; pair >= 0; pair--) {
    Card* blackjoker = new Card(mainwindow_, BJ, kSpade, pair);
    blackjoker->hide();
    origin_card_list_.append(blackjoker);
  }
  // 2 A K Q J 10 9 8 7 6 5 4 3 2 1
  for (int cardpoint = k2; cardpoint >= k3; cardpoint--)
    for (int pair = 1; pair >= 0; pair--)
      for (int cardsuit = kDiamond; cardsuit >= kSpade; cardsuit--) {
        Card* card = new Card(mainwindow_, static_cast<CardPoint>(cardpoint),
                              static_cast<CardSuit>(cardsuit), pair);
        card->hide();
        origin_card_list_.append(card);
      }
}

int Doudizhu::GetRandomNumber(const int& max) {
  qsrand(static_cast<uint>(time(nullptr)));
  return qrand() % max;
}

void Doudizhu::BidForLandlord() {
  volatile int first_bid = -1;
  volatile int last_bid = -1;
  int startform = GetRandomNumber(4);
  bid_landlord_mutex_.lock();
  flag_player_bided_ = false;
  flag_stop_bid_for_landlord_ = false;
  bid_landlord_mutex_.unlock();
  // Ask Player for bid.
  for (int i = 0; i != 4; i++) {
    volatile int rank = (startform + i) % 4;
    emit YouNeedToShowBidBtn(rank);
    while (true) {
      bid_landlord_mutex_.lock();
      if (flag_player_bided_) {
        if (player_is_bid_) {
          if (first_bid == -1) {
            first_bid = rank;
            last_bid = rank;
          } else
            last_bid = rank;
          qDebug() << rank << "Bid!";
        }
        flag_player_bided_ = false;
        bid_landlord_mutex_.unlock();
        break;  // Here will break! UNLOCK MUTEX FIRST!
      }
      bid_landlord_mutex_.unlock();
      if (flag_stop_bid_for_landlord_) return;
    }
  }
  // There is no one bid.
  if (first_bid == -1) {
    emit YouNeedToShuffleCard();
    return;
  }
  // If only one player bid, it isn't need to ask again.
  if (last_bid != first_bid) {
    emit YouNeedToShowBidBtn(first_bid);
    while (true) {
      bid_landlord_mutex_.lock();
      if (flag_player_bided_) {
        if (player_is_bid_) last_bid = first_bid;
        bid_landlord_mutex_.unlock();
        break;
      }
      bid_landlord_mutex_.unlock();
      if (flag_stop_bid_for_landlord_) return;
    }
  }
  emit YouNeedToSetLandlord(last_bid);
}

void Doudizhu::StartGame()
{
  if (online_player_.size() == 3)
  {
    if (!LoadsMatch()) {
      QMessageBox::critical(nullptr, "开启失败", "对局获取/创建失败");
      return;
    }
    if (!LoadsPlayersMatchScore()) {
      QMessageBox::critical(nullptr, "开启失败", "用户对局信息获取/创建失败");
      return;
    }
    transceiver_->TellClientLoadMatchAndMatchScore();
    mainwindow_->ClearAllPlayingCard();
    transceiver_->TellClientClearPlayingCard();
    ShuffleCard();
  }
}

void Doudizhu::SetLandlord(const int& landlord_rank) {
  if (landlord_rank == 0) {
    landlord_ = local_player_;
    for (int i = 7; i >= 0; i--) {
      local_player_->AddCard(card_list_[i]);
      card_list_.removeAt(i);
    }
  } else {
    landlord_ = online_player_[landlord_rank - 1];
    for (int i = 7; i >= 0; i--) {
      transceiver_->DealCardToClient(landlord_rank - 1,
                                     card_list_[i]->card_id());
      online_player_[landlord_rank - 1]->AddCardsSize();
      card_list_.removeAt(i);
    }
    emit YouNeedToDrawCard(landlord_rank - 1);
  }
  transceiver_->TellClientUpdateCardsSize();
  current_player_ = landlord_rank;
  // Set Current player first!!! call TellPlayerToPlayCard,
  // the client will use the current player!
  transceiver_->TellClientSetCurrentPlayer(landlord_rank);
  transceiver_->TellClientLandLordRank(landlord_rank);
  transceiver_->TellPlayerToPlayCard(landlord_rank);
}

void Doudizhu::Play(const QList<Card*>& played_card) {
  if (amiserver_) {
    // call TellClientPlayedCard() and
    // TellPlayerToPlayCard() to tell next player
    // show play button.
    transceiver_->TellClientPlayedCard(played_card);
    // Play() will be called before erasing, therefore
    // Judge whether the card has been played out.
    // Client will do count out in Transceiver Read() case 9.
    if (local_player_->cards_size() == played_card.size()) {
      CountOut();
    } else  // Next player
    {
      transceiver_->TellClientUpdateCardsSize();
      transceiver_->TellPlayerToPlayCard(1);
      current_player_++;
      current_player_ %= 4;
    }
  } else {
    transceiver_->TellServerPlayedCard(played_card);
    // When server send played cards to self, trun current
    // player.
  }
}

void Doudizhu::CountOut() {
  int landlord_win;  // 1:win; -1:lose;
  QList<Player*> player_list;
  player_list.append(local_player_);
  for (const auto& player : qAsConst(online_player_))
    player_list.append(player);
  if (player_list[current_player_] == landlord_)
    landlord_win = 1;
  else
    landlord_win = -1;
  for (const auto& player : qAsConst(player_list))
    player->set_money(player->money() - landlord_win * score_base_);
  landlord_->set_money(landlord_->money() + 4 * landlord_win * score_base_);
  mainwindow_->UpdateLabel();

  // Persistence
  recorder_->UpdateMatchResult(match_id_, local_player_->uuid().toRfc4122(),
                               local_player_->money());
  for (const auto& o_player : online_player_) {
    recorder_->UpdateMatchResult(match_id_, o_player->uuid().toRfc4122(),
                                 o_player->money());
  }

  // transceiver_->TellClinetUpdateInfo(*local_player_, online_player_);
  // Restart a new game
  if (amiserver_) {
    mainwindow_->ClearAllPlayingCard();
    transceiver_->TellClientClearPlayingCard();
    emit YouNeedToShuffleCard();
  }
}

void Doudizhu::UpdatePlayingCard(QList<Card*> card) {
  int current_player = Doudizhu::GetInstance()->current_player();
  mainwindow_->HidePlayingCard(current_player);
  playing_card_[current_player] = card;
  // draw cards in player's hand
  // Needn't draw self's cards in player's hand
  // NOTE:DrawCard():player range is [-1, 2]
  // draw playing cards
  mainwindow_->DrawPlayingCard(current_player);
  if (card.size() != 0)  // card is not kNullCardList
  {
    playcard_->SetCurrentMaxCard(current_player, card);
  }
}

QList<Card*>* Doudizhu::playing_card() { return playing_card_; }

QList<Card*>* Doudizhu::card_to_play_list() { return &card_to_play_list_; }

bool Doudizhu::LoadsMatch() {
  QByteArray match_uuid_byte;
  QByteArray player_uuid_byte[4];
  if (online_player_.size() != 3) {
    return false;
  }
  for (int i = 0; i != 3; i++) {
    player_uuid_byte[i] = online_player_[i]->uuid().toRfc4122();
  }
  player_uuid_byte[3] = local_player_->uuid().toRfc4122();
  for (int i = 0; i != player_uuid_byte[0].size(); i++) {
    char c;
    c = ((player_uuid_byte[0][i] >> 4) + (player_uuid_byte[1][i] >> 4) +
         (player_uuid_byte[2][i] >> 4) + (player_uuid_byte[3][i] >> 4))
        << 4;
    c |= ((player_uuid_byte[0][i] << 4) + (player_uuid_byte[1][i] << 4) +
          (player_uuid_byte[2][i] << 4) + (player_uuid_byte[3][i] << 4)) >>
         4;
    match_uuid_byte.push_back(c);
  }
  if (QUuid::fromRfc4122(match_uuid_byte).isNull()) {
    return false;
  }
  match_id_ = recorder_->GetMatchIDByMatchUUID(match_uuid_byte);
  if (match_id_ == -1) {
    return false;
  }
  return true;
}

bool Doudizhu::LoadsPlayersMatchScore() {
  int score;
  score = recorder_->GetPlayerMatchScore(match_id_,
                                         local_player_->uuid().toRfc4122());
  // Normally, player score cannot be -1
  if (score == -1) {
    return false;
  }
  local_player_->set_money(score);
  for (auto player : online_player_) {
    score =
        recorder_->GetPlayerMatchScore(match_id_, player->uuid().toRfc4122());
    if (score == -1) {
      return false;
    }
    player->set_money(score);
  }
  mainwindow_->UpdateLabel();
  // transceiver_->TellClinetUpdateInfo(*local_player_, online_player_);
  return true;
}

OnlinePlayer *Doudizhu::FindPlayerBySocket(const QTcpSocket *player_socket)
{
  for(auto player:online_player_){
    if(player->tcpsocket() == player_socket) {
      return player;
    }
  }
  return nullptr;
}

int Doudizhu::FindOnlinePlayerRank(const OnlinePlayer *online_player)
{
  for(int i=0;i!=online_player_.size();i++) {
    if(online_player_[i] == online_player) {
      return i;
    }
  }
  return -1;
}

void Doudizhu::StopBid() {
  // Stop server.
  player_is_bid_ = true;
  mainwindow_->HideBidButton();
  flag_stop_bid_for_landlord_ = true;
  // Stop Client.
  transceiver_->TellClientStopBid();
}

// This function is called at the beginning of each game.
// Reinitialize card_list_.
// Tell all player clear their cards(four local players' QList).
void Doudizhu::ShuffleCard() {
  card_list_ = origin_card_list_;
  qDebug() << card_list_.size();
  mainwindow_->ClearPlayerCard();
  mainwindow_->HidePlayButton();
  playcard_->ClearCurrentMaxCard();
  shuffle_interval_time->start(150);
}

void Doudizhu::ShuffleOneCard() {
  if (card_list_.size() == 8)  // Every player is given 25 cards.
  {
    shuffle_interval_time->stop();
    // BidForLandlord();
    QtConcurrent::run(this, &Doudizhu::BidForLandlord);
  } else {
    qsrand(static_cast<uint>(time(nullptr)));
    int n;
    n = qrand() % card_list_.size();
    local_player_->AddCard(card_list_[n]);
    qDebug() << n;
    card_list_.removeAt(n);
    for (int players = 0; players != 3; players++) {
      n = qrand() % card_list_.size();
      transceiver_->DealCardToClient(players, card_list_[n]->card_id());
      online_player_[players]->AddCardsSize();
      card_list_.removeAt(n);
      emit YouNeedToDrawCard(players);
    }
  }
}

void Doudizhu::btn_bid_clicked() {
  mainwindow_->HideBidButton();
  if (amiserver_) {
    player_is_bid_ = true;
    flag_player_bided_ = true;
  } else {
    transceiver_->TellServerIsBid(true);
  }
}

void Doudizhu::btn_no_bid_clicked() {
  mainwindow_->HideBidButton();
  if (amiserver_) {
    player_is_bid_ = false;
    flag_player_bided_ = true;
  } else {
    transceiver_->TellServerIsBid(false);
  }
}

void Doudizhu::btn_play_clicked() {
  if (playcard_->Play(card_to_play_list_)) {
    UpdatePlayingCard(card_to_play_list_);
    Play(card_to_play_list_);
    int size = card_to_play_list_.size();
    // Delete the selected card from local_player_->cards()
    for (int i = 0; i != size; i++) {
      int indexof = local_player_->cards()->indexOf(card_to_play_list_[i]);
      if (indexof != -1)
        local_player_->cards()->erase(local_player_->cards()->begin() +
                                      indexof);
    }
    mainwindow_->DrawCard(-1);
    if (amiserver_) transceiver_->TellClientUpdateCardsSize();
    mainwindow_->HidePlayButton();
    card_to_play_list_.clear();
  }
}

void Doudizhu::btn_pass_clicked() {
  if (current_player_ != playcard_->max_card_owner()) {
    Play(kNullCardList);
    mainwindow_->HidePlayButton();
  }
}
