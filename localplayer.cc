#include "localplayer.h"
#include<QDebug>


LocalPlayer::LocalPlayer()
{
}

LocalPlayer::LocalPlayer(QUuid &uuid, QString name,
                         int money)
    :Player(uuid, name, money)
{

}

LocalPlayer::LocalPlayer(QUuid &uuid, QString name, QByteArray photo, int money)
    :Player(uuid,name,photo,money)
{

}

void LocalPlayer::ClearCard()
{
  for(int i=0;i!=cards_.size();i++)
    cards_[i]->hide();
  cards_.clear();
}

void LocalPlayer::AddCard(Card* card)
{
  qDebug()<<"AddCard()";
  cards_.append(card);
  emit YouNeedToDrawCard(-1);
}

QList<Card*>* LocalPlayer:: cards()
{
  return &cards_;
}

int LocalPlayer::cards_size() const
{
  return cards_.size();
}
