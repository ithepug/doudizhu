#include "addlocalplayerwindow.h"
#include "ui_addlocalplayerwindow.h"

AddLocalPlayerwindow::AddLocalPlayerwindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::AddLocalPlayerwindow) {
  ui->setupUi(this);
  this->setWindowTitle("添加用户角色");
  picked_profile_ = new QPixmap(":/image/res/image/profile_default.png");
  QFile *profile_file = new QFile(":/image/res/image/profile_default.png");
  picked_profile_byte_ = profile_file->readAll();
  ui->pick_profile->setPixmap(picked_profile_->scaled(100, 100));
}

AddLocalPlayerwindow::~AddLocalPlayerwindow() {
  delete picked_profile_;
  delete ui;
}

void AddLocalPlayerwindow::on_btn_cancel_clicked() { this->close(); }

void AddLocalPlayerwindow::on_btn_confirm_clicked() {
  QString name = ui->lineEdit->text();
  if (name.size() > 10) {
    QMessageBox::warning(nullptr, "添加失败", "用户名长度大于10",
                         QMessageBox::Ok);
    return;
  }
  QUuid uuid = QUuid::createUuid();
  Recorder *recorder = Recorder::GetInstance();
  LocalPlayer player(uuid, name, picked_profile_byte_);
  if (!recorder->AddLocalPlayer(player)) {
    QMessageBox::warning(nullptr, "添加失败", "插入数据库失败",
                         QMessageBox::Ok);
  }
  emit NewLocalPlayer();
  this->deleteLater();
}

void AddLocalPlayerwindow::on_btn_pick_profile_clicked() {
  QFile *imageFile = nullptr;
  QByteArray bytes;
  QPixmap pixmap;
  while (true) {
    QString fileName = QFileDialog::getOpenFileName(
        this, tr("Open Image"), "", tr("Image Files (*.png *.jpg *.bmp)"));
    if (fileName.isEmpty()) {
      return;
    } else {
      qDebug() << fileName;
      imageFile = new QFile(fileName);
      if (!imageFile->open(QIODevice::ReadOnly)) {
        QMessageBox::critical(nullptr, "选择失败", "打开选择的图片失败",
                              QMessageBox::Ok);
        return;
      }
      qDebug() << imageFile->size();
      if (imageFile->size() > 10485760) {
        QMessageBox::warning(nullptr, "选择失败",
                             "选择的图片大于10MB，请重新选择", QMessageBox::Ok);
      } else {
        bytes = imageFile->readAll();
        if (!pixmap.loadFromData(bytes)) {
          QMessageBox::warning(nullptr, "选择失败",
                               "加载选择的图片出错，请重新选择",
                               QMessageBox::Ok);
        } else {
          pixmap = pixmap.scaled(100, 100, Qt::IgnoreAspectRatio);
          ui->pick_profile->setPixmap(pixmap);
          delete picked_profile_;
          picked_profile_ = new QPixmap(pixmap);
          QByteArray byte_tmp;
          QBuffer buffer(&byte_tmp);
          buffer.open(QIODevice::ReadWrite);
          pixmap.save(&buffer, "PNG");
          picked_profile_byte_ = byte_tmp;
          qDebug() << "resize size:" << picked_profile_byte_.size();
          break;
        }
      }
    }
  }
}
