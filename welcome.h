#ifndef DOUDIZHU_WELCOME_H_
#define DOUDIZHU_WELCOME_H_

#include <QMainWindow>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QTcpServer>
#include <QTcpSocket>

#include "ui_welcome.h"
#include "mainwindow.h"
#include "addlocalplayerwindow.h"
#include <doudizhu.h>

namespace Ui {
class Welcome;
}

class Welcome : public QMainWindow
{
  Q_OBJECT

public:
  explicit Welcome(QWidget *parent = nullptr);
  ~Welcome();

private:
  Ui::Welcome *ui;

public slots:
  void on_start_game_clicked();
  void on_create_server_clicked();
  void UpdateListWidget(QStringList string_list);
  void on_listWidget_itemDoubleClicked(QListWidgetItem *);
  void UpdateSelectRoleList();

private slots:
  void on_btn_add_role_clicked();

public:
  QByteArray getCurrentUuid();

private:
  Recorder *recorder_;
  QList<EasyPlayer> for_select_role_;
};

#endif // DOUDIZHU_WELCOME_H_
