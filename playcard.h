﻿#ifndef PLAYCARD_H
#define PLAYCARD_H

#include"card.h"
#include"transceiver.h"

#include<QObject>

struct SimpleCard   //To make it easier to compare card
{
  int type;
  int value;
  int size;
};

class PlayCard : public QObject
{
  Q_OBJECT
public:
  explicit PlayCard(QObject *parent = nullptr);
  bool Play(const QList<Card*> &card_to_play);
  void SetCurrentMaxCard(int owner,const QList<Card *> &card);
  void ClearCurrentMaxCard();
  int max_card_owner() const;

private:
  SimpleCard TypeJudge(const QList<Card*> &card_to_play);
  bool TJAA(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAAA(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAAACC(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJABCDE0(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAAAA0(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJRRBB(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAABBCC0(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAAABBB0(const QList<Card*> &card_to_play,SimpleCard &card);
  bool TJAAACCBBBEE0(const QList<Card*> &card_to_play,SimpleCard &card);
  bool CardCmp(const SimpleCard &card1,const SimpleCard &card2);


private:
  SimpleCard max_card_;
  int max_card_owner_;
};

#endif // PLAYCARD_H
