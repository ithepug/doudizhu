//Contains card's point, suit and picture.

#ifndef DOUDIZHU_CARD_H_
#define DOUDIZHU_CARD_H_

#include<QPixmap>
#include <QWidget>
#include <QMouseEvent>

class MainWindow;

enum CardPoint
{
  k3,
  k4,
  k5,
  k6,
  k7,
  k8,
  k9,
  k10,
  kJ,
  kQ,
  kK,
  kA,
  k2,
  BJ,
  RJ
};

enum CardSuit
{
  kSpade,
  kHeart,
  kClub,
  kDiamond
};

class Card : public QWidget
{
  Q_OBJECT

public:
  explicit Card(QWidget *parent,CardPoint card_point,CardSuit card_suit,int pair);
  explicit Card(QWidget *parent,int card_id);
  ~Card();
  void paintEvent(QPaintEvent*);
  int card_point() const;
  int card_suit() const;
  int card_id() const;
  void mouseReleaseEvent(QMouseEvent * event);

private:
  MainWindow* mainwindow_;
  CardPoint card_point_;
  CardSuit card_suit_;
  int card_id_;
  QPixmap card_pic_;
  static QImage kcard_pic_;
  static int kcard_pic_height;
  static int kcard_pic_width;
};

#endif // DOUDIZHU_CARD_H_
