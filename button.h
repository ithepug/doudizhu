#ifndef BUTTON_H
#define BUTTON_H

#include <QPushButton>

class Button : public QPushButton
{
  Q_OBJECT
public:
  explicit Button(QWidget *parent = nullptr);
  void SetImage(QString imageurl);

public:
  static const QSize* const kSize_;

};

#endif // BUTTON_H
