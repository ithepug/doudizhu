#include "transceiver.h"
#include "mainwindow.h"
#include <doudizhu.h>

#include<QFuture>
#include<QtConcurrent/QtConcurrent>

const bool Transceiver::MODE_SERVER = true;
const bool Transceiver::MODE_CLIENT = false;
const quint16 Transceiver::kPort_ = 666;

Transceiver *Transceiver::GetInstance()
{
  static Transceiver* unique_instance = new Transceiver(nullptr);
  return unique_instance;
}

void Transceiver::SetMode(const bool &mode)
{
  if(tcpserver_ == nullptr && tcpclient_ == nullptr){
    mode_ = mode;

    if(mode_ == MODE_SERVER) {
      tcpserver_ = new QTcpServer(nullptr);
      tcpserver_->listen(QHostAddress::Any, kPort_);

      // Broadcast
      Broadcast* broadcast = Broadcast::GetInstance();
      broadcast->SetServerName(QHostInfo::localHostName());
      broadcast->SetServerPort(tcpserver_->serverPort());
      QTimer* broadcast_timer = new QTimer();
      broadcast_timer->setInterval(1000);
      broadcast_timer->start();
      connect(broadcast_timer, SIGNAL(timeout()), broadcast,
              SLOT(SendBroadcastDatagram()));

      connect(tcpserver_, SIGNAL(newConnection()), this, SLOT(OnNewConnectionToServer()));
    } else if(mode_ == MODE_CLIENT) {
      tcpclient_ = new QTcpSocket(nullptr);
      connect(tcpclient_, SIGNAL(connected()), this, SLOT(OnConnectedToServer()));
      connect(tcpclient_, SIGNAL(readyRead()), this, SLOT(Read()));
    }
  } else {
    qDebug()<<"Transceiver::SetMode(): "<<"fail to setmode again!";
  }
}

void Transceiver::ConnectToHost(const QString &hostName, const quint16 &port)
{
  if(tcpclient_ != nullptr) {
    tcpclient_->connectToHost(hostName, port);
  }
}

Transceiver::Transceiver(QObject* parent):
                                            QObject (parent)
{

}

//Common functions for server and clients******************

//Slot function for signal "readyRead()".
//The first line of transmitted data is the data type mark:
//0:    C2S     localplayer info(uuid, name and photo)
//1:    S2C     other player info
//2:    S2C     card to deal
//3:    S2C     tell client clear card(signal)
//4:    S2C     tell client bid for landlord(signal)
//5:    C2S     is bid?
//6:    S2C     client online player cards size
//7:    S2C     tell client stop bid for landlord(signal)
//8:    S2C     tell client show play button(signal)
//9:    S2C     played card
//10:   C2S     played card
//11:   S2C     current play player
//12:   S2C     tell client clear playing card(signal)
//14:   S2C     tell client to load match and match score(signal)
//15:   S2C     tell client landlord rank
//16:   S2C     tell client others photo
void Transceiver::Read()
{
  QString mark;
  QTcpSocket* socket=static_cast<QTcpSocket*>(sender());
  while(socket->canReadLine())
  {
    mark=socket->readLine();
    qDebug()<<"mark"<<mark;
    switch (mark.toInt())
    {
    case 0:
    {
      OnlinePlayer player;
      player.set_tcpsocket(socket);
      if(socket->canReadLine()) {
        QByteArray uuid_byte = socket->readLine();
        uuid_byte = uuid_byte.chopped(1);
        QUuid uuid = QUuid::fromRfc4122(uuid_byte);
        if(!uuid.isNull()) {
          player.set_uuid(uuid);
        }
        if(socket->canReadLine()) {
          QString name = socket->readLine().chopped(1);
          player.set_name(name);
        }
        QByteArray photo = socket->readAll();
        player.set_photo(photo);

        Doudizhu::GetInstance()->onReceivedPlayerInfoFromClient(player);
      }
      break;
    }
    case 1:
    {
      Doudizhu* doudizhu = Doudizhu::GetInstance();
      for(int i=0;i!=3;i++)
      {        
        OnlinePlayer player;
        player.set_name(socket->readLine().chopped(1));
        QByteArray uuid_byte = socket->readLine().chopped(1);
        QUuid uuid = QUuid::fromRfc4122(uuid_byte);
        player.set_uuid(uuid);
        player.set_money(socket->readLine().toInt());
        doudizhu->onReceivedPlayerInfoFromServer(i, player);
      }
      break;
    }
    case 2:
    {
      qDebug()<<"case 2";
      int card_id=socket->readLine().toInt();
      qDebug()<<108-card_id;
      //NOTE:The origin_card_list_ is from big to small.
      //The rank opposite to card_id.
      Doudizhu::GetInstance()->onReceivedDealingCardFromServer(107 - card_id);
      break;
    }
    case 3:
    {
      Doudizhu::GetInstance()->onReceivedClearCardSignalFromServer();
      break;
    }
    case 4:
    {
      Doudizhu::GetInstance()->onReceivedToBidSignalFromServer();
      break;
    }
    case 5:
    {
      Doudizhu::GetInstance()->onReceivedBidResultFromClient(
          socket->readLine().toInt());
      qDebug()<<"get it";
      break;
    }
    case 6:
    {
      for(int i=0;i!=3;i++)
      {
        int card_size;
        card_size=socket->readLine().toInt();
        qDebug()<<"*"<<card_size;
        Doudizhu::GetInstance()->onReceivedPlayerCardSizeFromServer(i, card_size);
      }
      break;
    }
    case 7:
    {
      Doudizhu::GetInstance()->onReceivedStopBidSignalFromServer();
      qDebug()<<"STOP";
      break;
    }
    case 8:
    {
      qDebug()<<"READ 8";
      Doudizhu::GetInstance()->onReceivedToPlaySignalFromServer();
      break;
    }
    case 9:
    {
      QList<Card*> card=ReadCard(socket);
      Doudizhu::GetInstance()->onReceivedPlayedCardFromServer(card);
      break;
    }
    case 10:
    {
      QList<Card*> card=ReadCard(socket);
      Doudizhu::GetInstance()->onReceivedPlayedCardFromClient(card);
      break;
    }
    case 11:
    {
      int player_rank = socket->readLine().toInt();
      Doudizhu::GetInstance()->onReceivedCurrentPlayerFromServer(player_rank);
      break;
    }
    case 12:
    {
      Doudizhu::GetInstance()->onReceivedClearPlayingCardSignalFromServer();
      break;
    }
    case 14:
    {
      Doudizhu::GetInstance()->onReceivedLoadMatchSignalFromServer();
      break;
    }
    case 15:
    {
      int player_rank = socket->readLine().toInt();
      Doudizhu::GetInstance()->onReceivedLandLordFromServer(player_rank);
      qDebug()<<"15: landlord="<<player_rank;
      break;
    }
    case 16:
    {
      int rank = socket->readLine().toInt();
      int photo_byte_size = socket->readLine().toInt();
      if(photo_byte_size >= 0) {
        QByteArray photo_byte = socket->read(photo_byte_size);
        Doudizhu::GetInstance()->onReceivedPlayerPhotoFromServer(rank, photo_byte);
      } else {
        qDebug()<<"Read() case 16: rank out of range";
      }
    }
    }
  }
}

void Transceiver::OnNewConnectionToServer()
{
  qDebug()<< "Transceiver::OnNewConnectionToServer() " << "new connection";
  QTcpSocket* tcpsocket;
  Doudizhu* doudizhu = Doudizhu::GetInstance();
  tcpsocket = tcpserver_->nextPendingConnection();
  connect(tcpsocket, SIGNAL(disconnected()), this, SLOT(DisConnection()));
  connect(tcpsocket, SIGNAL(readyRead()), this, SLOT(Read()));
  if (doudizhu->online_player_size() < 3)  // Online player must below 3.
  {
    OnlinePlayer* online_player = new OnlinePlayer();

    online_player->set_tcpsocket(tcpsocket);
    doudizhu->online_player_add(online_player);
  } else {
    tcpsocket->disconnectFromHost();
  }
}

void Transceiver::DisConnection()
{
  emit YouNeedToStopBid();
  Doudizhu::GetInstance()->online_player_erase(dynamic_cast<QTcpSocket*>(sender()));
  sender()->deleteLater();
}

void Transceiver::OnConnectedToServer()
{
  Doudizhu::GetInstance()->OnConnectedToServer();
}

void Transceiver::WriteCard(const QList<Card *>& card, QTcpSocket *socket)
{
  int size=card.size();
  qDebug()<<"WriteCard():"<<size;
  socket->write(QString::number(size).toUtf8()+"\n");//Write card size
  for(int i=0;i!=size;i++)
    socket->write(QString::number(card[i]->card_id()).toUtf8()+"\n");//Write card id
}
//*********************************************************


//Special functions for clients.***************************
void Transceiver::TellServerLocalPlayerInfo(const Player &player)
{
  tcpclient_->write("0\n");//data type mark
  tcpclient_->write(player.uuid().toRfc4122() + '\n');
  tcpclient_->write(player.name().toUtf8() + "\n");
  tcpclient_->write(player.photo());
  tcpclient_->flush();
}

void Transceiver::TellServerIsBid(bool isbid)
{
  tcpclient_->write("5\n");
  if(isbid)
    tcpclient_->write("1\n");
  else
    tcpclient_->write("0\n");
}

void Transceiver::HideClientBidButton()
{
  MainWindow::GetInstance(nullptr)->btn_bid_->hide();
  MainWindow::GetInstance(nullptr)->btn_no_bid_->hide();
}

void Transceiver::TellServerPlayedCard(const QList<Card *> &played_card)
{
  tcpclient_->write("10\n");
  WriteCard(played_card,tcpclient_);
}
//*********************************************************


//Special functions for server.****************************

//Functions for transmitting information that updates the
//client's information of label.
//Use delivered variables and blank players to compose the
//four players'information.
//Wirting to existed sockets the four players' information.
void Transceiver::TellClinetUpdateInfo(const LocalPlayer& server,const QList<OnlinePlayer*>& clients)
{
  Player player_server;
  player_server.set_name(server.name());
  player_server.set_money(server.money());
  player_server.set_uuid(server.uuid());
  QList<Player*> player_list;
  for(int i=0;i!=clients.size();i++)
    player_list.append(clients[i]);
  for(int i=clients.size();i!=3;i++)//Fill up to three players with blank player.
    player_list.append(Doudizhu::GetInstance()->blank_player());
  player_list.append(&player_server);
  for(int i=0;i!=clients.size();i++)//NOTE:clients size() is NOT equal to player_list size().
  {
    clients[i]->tcpsocket()->write("1\n");//data type mark
    for(int j=1;j!=4;j++)
    {
      clients[i]->tcpsocket()->write(player_list[(i+j)%4]->name().toUtf8()+'\n');
      clients[i]->tcpsocket()->write(player_list[(i+j)%4]->uuid().toRfc4122()+'\n');
      clients[i]->tcpsocket()->write(QString::number(player_list[(i+j)%4]->money()).toUtf8()+'\n');
      clients[i]->tcpsocket()->flush();
    }
  }
}

//Functions for Server Dealing card to Client.
//Passing players(0,1,2) and card_id(0-107).
void Transceiver::DealCardToClient(int players, int card_id)
{
  Doudizhu::GetInstance()->online_player_tcpsocket(players)->write("2\n");
  Doudizhu::GetInstance()->online_player_tcpsocket(players)->write(QString::number(card_id).toUtf8()+'\n');
}

void Transceiver::ClearAllClientCard()
{
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("3\n");
  }
}

void Transceiver::TellClinetShowBidButton(OnlinePlayer *client)
{
  client->tcpsocket()->write("4\n");
}

void Transceiver::TellClientUpdateCardsSize()
{
  for(int player=0;player!=Doudizhu::GetInstance()->online_player_size();player++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(player)->write("6\n");
    for (int i=1;i!=4;i++)
    {
      int rank=(player+i)%4;
      if(rank==3)
        Doudizhu::GetInstance()->online_player_tcpsocket(player)->write(
            QString::number(Doudizhu::GetInstance()->local_player_card_size()).toUtf8()+"\n");
      else
        Doudizhu::GetInstance()->online_player_tcpsocket(player)->write(
            (QString::number(Doudizhu::GetInstance()->online_player_card_size(rank)).toUtf8())+"\n");
    }
  }
}

void Transceiver::TellClientStopBid()
{
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("7\n");
  }
}

//Tell player show the play button for they to play.
void Transceiver::TellPlayerToPlayCard(const int &player)
{
  switch (player)
  {
  case 0:
    MainWindow::GetInstance(nullptr)->ShowPlayButton();
    break;
  default:
    Doudizhu::GetInstance()->online_player_tcpsocket(player -1)->write("8\n");
    qDebug()<<"NEXT Player"<<player;
    break;
  }
}

//Tell all player the card someone played.
void Transceiver::TellClientPlayedCard(const QList<Card *>& played_card)
{
  qDebug()<<"TellClientPlayedCard:"<<played_card.size();
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("9\n");
    WriteCard(played_card,Doudizhu::GetInstance()->online_player_tcpsocket(i));
  }
}

QList<Card *> Transceiver::ReadCard(QTcpSocket* socket)
{
  QList<Card *> card;
  int size=socket->readLine().toInt();
  qDebug()<<"ReadCard():"<<size;
  for(int i=0;i!=size;i++)
  {
    card.append(Doudizhu::GetInstance()->origin_card_list()[107-socket->readLine().toInt()]);
  }
  return card;
}

void Transceiver::TellClientSetCurrentPlayer(const int &player_rank)
{
  int rank;
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("11\n");
    rank=(player_rank-i+3)%4;
    qDebug()<<"set current plaer"<<rank;
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write(QString::number(rank).toUtf8()+"\n");
  }
}

void Transceiver::TellClientClearPlayingCard()
{
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("12\n");
  }
}

void Transceiver::TellClientLoadMatchAndMatchScore()
{
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("14\n");
  }
}

void Transceiver::TellClientLandLordRank(const int &rank)
{
  int landlord_rank;
  for(int i=0;i!=Doudizhu::GetInstance()->online_player_size();i++)
  {
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write("15\n");
    landlord_rank=(rank-i+3)%4;
    qDebug()<<"TellClientLandLordRank()"<<rank;
    Doudizhu::GetInstance()->online_player_tcpsocket(i)->write(QString::number(landlord_rank).toUtf8()+"\n");
  }
}

// NOTE: rank [0:3], 3 => localplayer
void Transceiver::TellAllClientProfile(const int &rank, const QByteArray &photo)
{
  Doudizhu* doudizhu = Doudizhu::GetInstance();
  QList<OnlinePlayer*> clients;
  for(int i=0;i!=doudizhu->online_player_size();i++)
  {
    OnlinePlayer* online_player = new OnlinePlayer;
    online_player->set_tcpsocket(doudizhu->online_player_tcpsocket(i));
    clients.append(online_player);
  }

  for(int i=0;i!=clients.size();i++)
  {
    if(i != rank) {
      if(clients[i] != nullptr && clients[i]->tcpsocket() != nullptr &&
          clients[i]->tcpsocket()->isWritable()) {
        clients[i]->tcpsocket()->write("16\n");
        clients[i]->tcpsocket()->write(QString::number((rank - i + 4)%4 - 1).toUtf8() + "\n");
        clients[i]->tcpsocket()->write(QString::number(photo.size()).toUtf8() + "\n");
        clients[i]->tcpsocket()->write(photo);
        clients[i]->tcpsocket()->flush();
      }
    }
  }

  // Clear
  qDeleteAll(clients);
}

// NOTE: rank [0:3], 3 => localplayer
void Transceiver::TellClientAllProfile(const int &rank, const QList<QByteArray> &photos)
{
  Doudizhu* doudizhu = Doudizhu::GetInstance();
  QList<OnlinePlayer*> clients;
  QList<Player*> player_list;
  for(int i=0;i!=photos.size();i++)
  {
    OnlinePlayer* online_player = new OnlinePlayer;
    online_player->set_photo(photos[i]);
    online_player->set_tcpsocket(doudizhu->online_player_tcpsocket(i));
    clients.append(online_player);
    player_list.append(online_player);
  }

  //Fill up to three players with blank player.
  for(int i=clients.size();i!=3;i++)
    player_list.append(Doudizhu::GetInstance()->blank_player());

  LocalPlayer* local_player = new LocalPlayer;
  local_player->set_photo(doudizhu->local_player_photo());
  player_list.append(dynamic_cast<Player*>(local_player));

  for(int i=0;i!=player_list.size();i++)
  {
    if(i != rank) {
      if(clients[rank] != nullptr && clients[rank]->tcpsocket() != nullptr
          && clients[rank]->tcpsocket()->isWritable()) {
        if(player_list[i] != Doudizhu::GetInstance()->blank_player()) {
          clients[rank]->tcpsocket()->write("16\n");
          clients[rank]->tcpsocket()->write(QString::number((i - rank + 4)%4 - 1).toUtf8() + "\n");
          clients[rank]->tcpsocket()->write(QString::number(player_list[i]->photo().size()).toUtf8() + "\n");
          clients[rank]->tcpsocket()->write(player_list[i]->photo());
          clients[rank]->tcpsocket()->flush();
        }
      }
    }
  }

  // Clear
  qDeleteAll(clients);
  delete local_player;
}

//*********************************************************
