四人斗地主游戏
=
##  **Doudizhu**
> 负责整个斗地主游戏的主流程

* ### 流程图
    ![](./doc/doudizhu-flow.png)

* ### 回调函数
    |函数|描述|
    |-----|----|
    |void onReceivedPlayerInfoFromClient(const OnlinePlayer& player);|收到来自客户端的玩家信息|
    |void onReceivedBidResultFromClient(const bool& isBid);|收到来自客户端的是否抢地主的结果|
    |void onReceivedPlayedCardFromClient(const QList<Card*>& cards);|收到来自客户端玩家打出的牌|
    |void onReceivedPlayerInfoFromServer(const int& index, const OnlinePlayer& player);|收到来自服务器的其他玩家的信息|
    |void onReceivedDealingCardFromServer(const int& card_index);|收到来自服务器发的牌|
    |void onReceivedClearCardSignalFromServer();|收到来自服务器的清空手中的牌的信号|
    |void onReceivedToBidSignalFromServer();|收到来自服务器的去抢地主的信号|
    |void onReceivedPlayerCardSizeFromServer(const int& index, const int& size);|收到来自服务器的其他玩家手中牌的数量|
    |void onReceivedStopBidSignalFromServer();|收到来自服务器的停止抢地主的信号|
    |void onReceivedToPlaySignalFromServer();|收到来自服务器的去打出牌的信号|
    |void onReceivedPlayedCardFromServer(const QList<Card*>& cards);|收到来自服务器的其他玩家打出的牌|
    |void onReceivedCurrentPlayerFromServer(const int& index);|收到来自服务器的当前玩家是谁|
    |void onReceivedClearPlayingCardSignalFromServer();|收到来自服务器的清空被打出的牌的信号|
    |void onReceivedLoadMatchSignalFromServer();|收到来自服务器 的去加载对局信息的信号|
    |void onReceivedLandLordFromServer(const int& index);|收到来自服务器的地主是谁|
    |void onReceivedPlayerPhotoFromServer(const int& index, const QByteArray& photo);|收到来自服务器的其他玩家的头像|
    |void OnConnectedToServer();|成功连接到服务器|


##  **Transceiver**
> 负责服务器与客户端的通信

* ### 通信示例图
    ![](./doc/transceiver-communication.png)