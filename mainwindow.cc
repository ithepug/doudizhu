#include "mainwindow.h"
#include "card.h"
#include "ui_mainwindow.h"

#include <doudizhu.h>

#include <QFile>
#include <QFuture>
#include <QHostInfo>
#include <QPainter>
#include <QTcpSocket>
#include <QtConcurrent/QtConcurrent>

// Overload function for new server.
MainWindow::MainWindow(Doudizhu *doudizhu, QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  doudizhu_ = doudizhu;
  transceiver_ = Transceiver::GetInstance();
  ui->setupUi(this);
  Init();
  InitConnection();
}

// Overload function for new client.
MainWindow::MainWindow(Doudizhu *doudizhu, QString server_ip, QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  doudizhu_ = doudizhu;
  transceiver_ = Transceiver::GetInstance();
  qDebug() << "T";
  ui->setupUi(this);
  Init();
  connect(transceiver_, SIGNAL(YouNeedToUpdateLabel()), this,
          SLOT(UpdateLabel()));
  InitConnection();
}

MainWindow *MainWindow::GetInstance(Doudizhu *doudizhu, QString server_ip) {
  static MainWindow *unique_instance;
  if (doudizhu != nullptr) {
    if (server_ip.isEmpty())  // Server mode
    {
      unique_instance = new MainWindow(doudizhu);
    } else  // Client mode
    {
      unique_instance = new MainWindow(doudizhu, server_ip);
    }
  }
  return unique_instance;
}

MainWindow::~MainWindow() { delete ui; }

// Initialization.
// Call InitCard(), InitLable() and InitConnection.
void MainWindow::Init() {
  border_top_ = 15;
  status_bar_top_ = height() - 70;
  primary_area_left_ = 200;
  primary_area_top_ = 145;
  primary_area_right_ = width() - 200;
  primary_area_bottom_ = height() - 255;

  btn_centre_point_ = QPoint(width() / 2, primary_area_bottom_ - 65);

  kCardSizeL_ = new QSize(116, 160);
  kCardSizeS_ = new QSize(86, 120);

  card_back_right_left_ =
      new QPixmap(":/image/res/image/card_back_right_left.png");
  card_back_top_ = new QPixmap(":/image/res/image/card_back_top.png");
  online_player_card_back_pic_[0] = new QLabel(this);
  online_player_card_back_pic_[0]->resize(kCardSizeS_->width(), 440);
  online_player_card_back_pic_[0]->setAlignment(Qt::AlignTop);
  online_player_card_back_pic_[1] = new QLabel(this);
  online_player_card_back_pic_[1]->resize(566, kCardSizeS_->height());
  online_player_card_back_pic_[2] = new QLabel(this);
  online_player_card_back_pic_[2]->resize(kCardSizeS_->width(), 440);
  online_player_card_back_pic_[2]->setAlignment(Qt::AlignTop);

  default_profile_ = new QPixmap(":/image/res/image/profile_default.png");
  user_profile_[0] = default_profile_;
  user_profile_[1] = default_profile_;
  user_profile_[2] = default_profile_;
  user_profile_[3] = default_profile_;

  InitLabel();
  InitPushButton();
}

// Initialization of Labels(local player's name and money,
// game multiple,three online players'name and money).
void MainWindow::InitLabel() {
  label_local_player_name_ = new QLabel(this);
  label_local_player_name_->move(100, height() - 38);
  label_local_player_name_->setStyleSheet("color:white; font:22px 迷你简准圆");
  label_local_player_money_ = new QLabel(this);
  label_local_player_money_->move(550, height() - 38);
  label_local_player_money_->setStyleSheet(
      "color:yellow; font:26px 迷你简准圆");
  label_online_player_name_[0] = new QLabel(this);
  label_online_player_name_[0]->setAlignment(Qt::AlignRight);
  label_online_player_name_[0]->move(primary_area_right_ + 10,
                                     primary_area_top_ - 55);
  label_online_player_name_[0]->setStyleSheet(
      "color:white; font:18px 迷你简准圆 ");
  label_online_player_money_[0] = new QLabel(this);
  label_online_player_money_[0]->setAlignment(Qt::AlignRight);
  label_online_player_money_[0]->move(primary_area_right_ + 10,
                                      primary_area_top_ - 25);
  label_online_player_money_[0]->setStyleSheet(
      "color:yellow; font:18px 迷你简准圆 ");
  label_online_player_name_[1] = new QLabel(this);
  label_online_player_name_[1]->setAlignment(Qt::AlignLeft);
  label_online_player_name_[1]->move(primary_area_left_ + 170,
                                     border_top_ + 15);
  label_online_player_name_[1]->setStyleSheet(
      "color:white; font:18px 迷你简准圆 ");
  label_online_player_money_[1] = new QLabel(this);
  label_online_player_money_[1]->setAlignment(Qt::AlignLeft);
  label_online_player_money_[1]->move(primary_area_left_ + 170,
                                      border_top_ + 45);
  label_online_player_money_[1]->setStyleSheet(
      "color:yellow; font:18px 迷你简准圆 ");
  label_online_player_name_[2] = new QLabel(this);
  label_online_player_name_[2]->setAlignment(Qt::AlignLeft);
  label_online_player_name_[2]->move(90, primary_area_top_ - 55);
  label_online_player_name_[2]->setStyleSheet(
      "color:white; font:18px 迷你简准圆 ");
  label_online_player_money_[2] = new QLabel(this);
  label_online_player_money_[2]->setAlignment(Qt::AlignLeft);
  label_online_player_money_[2]->move(90, primary_area_top_ - 25);
  label_online_player_money_[2]->setStyleSheet(
      "color:yellow; font:18px 迷你简准圆 ");
  label_score_base_ = new QLabel(this);
  label_score_base_->move(width() - 650, height() - 38);
  label_score_base_->setStyleSheet("color:yellow; font:26px 迷你简准圆");
}

// Initialization of Button.
void MainWindow::InitPushButton() {
  btn_bid_ = new Button(this);
  btn_bid_->SetImage(":/image/res/image/btn_bid.png");
  btn_bid_->move(btn_centre_point_.x() - btn_bid_->kSize_->width() - 25,
                 btn_centre_point_.y());
  btn_no_bid_ = new Button(this);
  btn_no_bid_->SetImage(":/image/res/image/btn_no_bid.png");
  btn_no_bid_->move(btn_bid_->pos().x() + btn_no_bid_->kSize_->width() + 50,
                    btn_bid_->pos().y());
  btn_play_ = new Button(this);
  btn_play_->SetImage(":/image/res/image/btn_play.png");
  btn_play_->move(
      btn_centre_point_.x() - 3 * btn_bid_->kSize_->width() / 2 - 25,
      btn_centre_point_.y());
  btn_pass_ = new Button(this);
  btn_pass_->SetImage(":/image/res/image/btn_pass.png");
  btn_pass_->move(btn_centre_point_.x() - btn_bid_->kSize_->width() / 2,
                  btn_centre_point_.y());
  btn_prompt_ = new Button(this);
  btn_prompt_->SetImage(":/image/res/image/btn_prompt.png");
  btn_prompt_->move(btn_centre_point_.x() + btn_bid_->kSize_->width() / 2 + 25,
                    btn_centre_point_.y());
}

void MainWindow::InitConnection() {
  connect(this, SIGNAL(YouNeedToDrawCard(int)), this, SLOT(DrawCard(int)));
  connect(transceiver_, SIGNAL(YouNeedToDrawCard(int)), this,
          SLOT(DrawCard(int)));
}

// Draw the basic layout.
void MainWindow::paintEvent(QPaintEvent *) {
  QPainter p(this);
  p.drawPixmap(this->rect(), QPixmap(":/image/res/image/game_bg.png"));
  p.drawPixmap(0, height() - 50, width(), 50,
               QPixmap(":/image/res/image/status_bar_bg.png"));
  p.drawPixmap(width() - 300, height() - 60,
               QPixmap(":/image/res/image/status_bar_info_bg.png"));
  p.drawPixmap(
      0, height() - 60,
      QPixmap().fromImage(QImage(":/image/res/image/status_bar_info_bg.png")
                              .mirrored(true, false)));
  p.drawPixmap(35, height() - 40, QPixmap(":/image/res/image/info_box_bg.png"));
  p.drawPixmap(500, height() - 40,
               QPixmap(":/image/res/image/info_box_bg.png"));
  p.drawPixmap(500, height() - 40, QPixmap(":/image/res/image/money_icon.png"));
  p.drawPixmap(width() - 700, height() - 40,
               QPixmap(":/image/res/image/info_box_bg.png"));
  p.drawPixmap(width() - 700, height() - 40,
               QPixmap(":/image/res/image/multiple_icon.png"));
  p.drawPixmap(-5, height() - 96,
               QPixmap(":/image/res/image/profile_picture_bg.png"));

  p.drawPixmap(primary_area_right_, primary_area_top_ - 70,
               QPixmap(":/image/res/image/online_player_info_bg.png"));
  p.drawPixmap(
      primary_area_left_ + 100, border_top_,
      QPixmap().fromImage(QImage(":/image/res/image/online_player_info_bg.png")
                              .mirrored(true, false)));
  p.drawPixmap(
      primary_area_left_ - 180, primary_area_top_ - 70,
      QPixmap().fromImage(QImage(":/image/res/image/online_player_info_bg.png")
                              .mirrored(true, false)));

  p.drawPixmap(2, height() - 89, 86, 86, *user_profile_[0]);
  p.drawPixmap(primary_area_right_ + 116, primary_area_top_ - 64, 59, 59,
               *user_profile_[1]);
  p.drawPixmap(primary_area_left_ + 105, border_top_ + 6, 59, 59,
               *user_profile_[2]);
  p.drawPixmap(primary_area_left_ - 175, primary_area_top_ - 64, 59, 59,
               *user_profile_[3]);
}

// Function of window size change event.
// Ensure correct layout.
void MainWindow::resizeEvent(QResizeEvent *) {
  status_bar_top_ = height() - 70;
  primary_area_right_ = width() - 200;
  primary_area_bottom_ = height() - 255;
  btn_centre_point_ = QPoint(width() / 2, primary_area_bottom_ - 65);

  label_local_player_name_->move(100, height() - 38);
  label_local_player_money_->move(550, height() - 38);
  label_score_base_->move(width() - 650, height() - 38);

  label_online_player_name_[0]->move(primary_area_right_ + 10,
                                     primary_area_top_ - 55);
  label_online_player_money_[0]->move(primary_area_right_ + 10,
                                      primary_area_top_ - 25);
  // Button
  btn_play_->move(
      btn_centre_point_.x() - 3 * btn_bid_->kSize_->width() / 2 - 25,
      btn_centre_point_.y());
  btn_pass_->move(btn_centre_point_.x() - btn_bid_->kSize_->width() / 2,
                  btn_centre_point_.y());
  btn_prompt_->move(btn_centre_point_.x() + btn_bid_->kSize_->width() / 2 + 25,
                    btn_centre_point_.y());
  btn_bid_->move(btn_centre_point_.x() - btn_bid_->kSize_->width() - 25,
                 btn_centre_point_.y());
  btn_no_bid_->move(btn_bid_->pos().x() + btn_no_bid_->kSize_->width() + 50,
                    btn_bid_->pos().y());
  // Cards on hand
  if (doudizhu_->online_player_size() == 3) {
    emit YouNeedToDrawCard(-1);
    emit YouNeedToDrawCard(0);
    emit YouNeedToDrawCard(1);
    emit YouNeedToDrawCard(2);
  }
  // Playing cards
  for (int i = 0; i != 4; i++) {
    DrawPlayingCard(i);
  }
}

// Function for Drawing all players' info label.
void MainWindow::DrawPlayer(const Player &player) {
  const OnlinePlayer *index = &static_cast<const OnlinePlayer &>(player);
  int indexof =
      doudizhu_->FindOnlinePlayerRank(const_cast<OnlinePlayer *>(index));
  if (indexof == -1)  // Player is local player
  {
    label_local_player_name_->setText(player.name());
    label_local_player_money_->setNum(player.money());
  } else {
    label_online_player_name_[indexof]->setText(player.name());
    if (player.money() == -1)  // Player is the blank player.
      label_online_player_money_[indexof]->setText("");
    else
      label_online_player_money_[indexof]->setNum(player.money());
  }
}

// Function for update all players' info label.
void MainWindow::UpdateLabel() {
  QList<OnlinePlayer *> online_player = doudizhu_->online_player_;
  LocalPlayer *local_player = doudizhu_->local_player_;
  DrawPlayer(*local_player);
  for (int i = 0; i != online_player.size(); i++) DrawPlayer(*online_player[i]);
  for (int i = online_player.size(); i != 3; i++)  // clear blank players' label
  {
    label_online_player_name_[i]->clear();
    label_online_player_money_[i]->clear();
  }
}

// Function for clear all player's cards.
// Clear local player's cards(List).
// Zero onlineplayers' size of cards(int).
// Tell client to clear their local player's card
// and zero their online players' size fo cards.
void MainWindow::ClearPlayerCard() {
  QList<OnlinePlayer *> online_player = doudizhu_->online_player_;
  LocalPlayer *local_player = doudizhu_->local_player_;
  local_player->ClearCard();
  for (int i = 0; i != online_player.size(); i++)
    online_player[i]->ZeroingCardsSize();
  transceiver_->ClearAllClientCard();  // NOTE:call only online player = 3
}

// Function for Drawing four players' cards picture.
// When Draw local player's card, it will sort local player's
// cards at first.
void MainWindow::DrawCard(const int &player) {
  QList<OnlinePlayer *> online_player = doudizhu_->online_player_;
  LocalPlayer *local_player = doudizhu_->local_player_;
  int distance;
  int start;
  int cutsize;
  switch (player) {
  case -1:
    qDebug() << "DrawCard(-1)";
    if (local_player->cards()->size() != 0) {
      distance = 35;
      start = (width() - (distance * (local_player->cards()->size() - 1) +
                          kCardSizeL_->width())) /
              2;
      (*local_player->cards())[local_player->cards()->size() - 1]->show();
      std::sort(local_player->cards()->begin(), local_player->cards()->end(),
                CardCmp);
      int card_size = local_player->cards()->size();
      for (int i = 0; i != card_size; i++) {
        ResizeCard2SizeL(*(*local_player->cards())[i]);
        (*local_player->cards())[i]->move(start + (i * distance),
                                          status_bar_top_ - 160);
      }
      card_size = doudizhu_->card_to_play_list()->size();
      for (int i = 0; i != card_size; i++)
        (*doudizhu_->card_to_play_list())[i]->move(
            (*doudizhu_->card_to_play_list())[i]->x(),
            (*doudizhu_->card_to_play_list())[i]->y() - 25);
    }
    break;
  case 0: {
    if (online_player[player]->cards_size() != 0) {
      distance = 10;
      start = (primary_area_top_ + primary_area_bottom_) / 2 -
              ((10 * (online_player[player]->cards_size() - 1) + 120) / 2);
      cutsize = (33 - online_player[player]->cards_size()) * distance;
      online_player_card_back_pic_[player]->setPixmap(
          RoundCorner(card_back_right_left_->copy(
              0, cutsize, kCardSizeS_->width(), 440 - cutsize)));
      online_player_card_back_pic_[player]->move(width() - 143, start);
    } else {
      online_player_card_back_pic_[player]->clear();
    }
    break;
  }
  case 1:
    if (online_player[player]->cards_size() != 0) {
      distance = 15;
      start = width() / 2 -
              ((15 * (online_player[player]->cards_size() - 1) + 86) / 2);
      cutsize = (33 - online_player[player]->cards_size()) * distance;
      online_player_card_back_pic_[player]->setPixmap(
          RoundCorner(card_back_top_->copy(cutsize, 0, 566 - cutsize,
                                           kCardSizeS_->height())));
      online_player_card_back_pic_[player]->move(start, border_top_);
    } else {
      online_player_card_back_pic_[player]->clear();
    }
    break;
  case 2:
    if (online_player[player]->cards_size() != 0) {
      distance = 10;
      start = (primary_area_top_ + primary_area_bottom_) / 2 -
              ((10 * (online_player[player]->cards_size() - 1) + 120) / 2);
      cutsize = (33 - online_player[player]->cards_size()) * distance;
      online_player_card_back_pic_[player]->setPixmap(
          RoundCorner(card_back_right_left_->copy(
              0, cutsize, kCardSizeS_->width(), 440 - cutsize)));
      online_player_card_back_pic_[player]->move(57, start);
    } else {
      online_player_card_back_pic_[player]->clear();
    }
    break;
  }
}

// Function for DrawCard() to draw card picture in order.
bool MainWindow::CardCmp(const Card *card1, const Card *card2) {
  return card1->card_id() > card2->card_id();
}

// Function for DrawCard() to rounded card's picture.
QPixmap MainWindow::RoundCorner(const QPixmap &src) {
  QPixmap rounded(src.width(), src.height());
  rounded.fill(Qt::transparent);
  QPainter p(&rounded);
  QPainterPath path;
  path.addRoundedRect(QRectF(0, 0, src.width(), src.height()), 8, 8);
  p.setRenderHints(QPainter::Antialiasing, true);
  p.setClipPath(path);
  p.drawPixmap(0, 0, src);
  return rounded;
}

void MainWindow::ShowBidButton(int rank) {
  QList<OnlinePlayer *> online_player = doudizhu_->online_player_;
  switch (rank) {
  case 0:
    btn_bid_->show();
    btn_no_bid_->show();
    break;
  default:
    transceiver_->TellClinetShowBidButton(online_player[rank - 1]);
    break;
  }
}

void MainWindow::ShowPlayButton() {
  int current_player = doudizhu_->current_player();
  HidePlayingCard(current_player);
  btn_play_->show();
  btn_pass_->show();
  btn_prompt_->show();
}

void MainWindow::HidePlayButton() {
  btn_play_->hide();
  btn_pass_->hide();
  btn_prompt_->hide();
}

// When mouse clicked,Card::mouseReleaseEvent() will call
// this function.
void MainWindow::SelectCard(Card *card_begin, int size) {
  LocalPlayer *local_player = doudizhu_->local_player_;
  int rank_begin = (*local_player->cards()).indexOf(card_begin);
  int local_player_cards_size = local_player->cards_size();
  int mark = size > 0 ? 1 : -1;
  for (int i = 0; i != size; i += mark) {
    if (rank_begin + i == -1 || rank_begin + i == local_player_cards_size)
      break;
    Card *selected_card = (*local_player->cards())[rank_begin + i];
    if (selected_card->y() == status_bar_top_ - 160)
      AddCardToPlay(selected_card);
    else
      DelCardToPlay(selected_card);
  }
}

void MainWindow::AddCardToPlay(Card *card) {
  card->move(card->x(), card->y() - 25);
  doudizhu_->AddCardToPlay(card);
}

void MainWindow::DelCardToPlay(Card *card) {
  card->move(card->x(), card->y() + 25);
  doudizhu_->DelCardToPlay(card);
}

void MainWindow::HideBidButton() {
  btn_bid_->hide();
  btn_no_bid_->hide();
}

void MainWindow::ClearLabel(const int &player) {
  label_online_player_name_[player]->clear();
  label_online_player_money_[player]->clear();
}

void MainWindow::DrawPlayingCard(int player) {
  int spacing = 25;
  int startfrom_x = 0;
  int startfrom_y = 0;
  int size = doudizhu_->playing_card()[player].size();
  qDebug() << "Draw playing card current player:" << player;
  switch (player) {
  case 0:
    startfrom_x = (width() - kCardSizeS_->width() - (size - 1) * spacing) / 2;
    startfrom_y = primary_area_bottom_ - kCardSizeS_->height();
    break;
  case 1:
    startfrom_x =
        primary_area_right_ - kCardSizeS_->width() - (size - 1) * spacing;
    startfrom_y =
        (primary_area_top_ + primary_area_bottom_ - kCardSizeS_->height()) /
        2;
    break;
  case 2:
    startfrom_x = (width() - kCardSizeS_->width() - (size - 1) * spacing) / 2;
    startfrom_y = primary_area_top_;
    break;
  case 3:
    startfrom_x = primary_area_left_;
    startfrom_y =
        (primary_area_top_ + primary_area_bottom_ - kCardSizeS_->height()) /
        2;
    break;
  }
  for (int i = 0; i != size; i++) {
    doudizhu_->playing_card()[player][i]->resize(*kCardSizeS_);  // Resize Card
    doudizhu_->playing_card()[player][i]->move(startfrom_x + i * spacing,
                                               startfrom_y);
    doudizhu_->playing_card()[player][i]->show();
  }
}

void MainWindow::HidePlayingCard(int player) {
  int size = doudizhu_->playing_card()[player].size();
  for (int i = 0; i != size; i++) doudizhu_->playing_card()[player][i]->hide();
}

void MainWindow::ClearAllPlayingCard() {
  const QList<Card *> nullcard;
  for (int i = 0; i != 4; i++) {
    HidePlayingCard(i);
    doudizhu_->playing_card()[i] = nullcard;
    DrawPlayingCard(i);
  }
}

void MainWindow::UpdateScoreBaseLabel(const int &base) {
  label_score_base_->setText(QString::number(base));
}

void MainWindow::ResizeCard2SizeL(Card &card) { card.resize(*kCardSizeL_); }

void MainWindow::ResizeCard2SizeS(Card &card) { card.resize(*kCardSizeL_); }

bool MainWindow::SetPlayerProfile(const int &player,
                                  const QByteArray &profile) {
  QPixmap *profile_pixmap = new QPixmap;
  if (!profile_pixmap->loadFromData(profile)) return false;
  if (user_profile_[player] != default_profile_) delete user_profile_[player];

  QPixmap profile_tmp(*profile_pixmap);
  profile_pixmap->fill(Qt::transparent);
  QPainter p(profile_pixmap);
  QPainterPath path;
  path.addRoundedRect(
      QRectF(0, 0, profile_pixmap->width(), profile_pixmap->height()),
      profile_pixmap->width() / 2, profile_pixmap->height() / 2);
  p.setRenderHints(QPainter::Antialiasing);
  p.setClipPath(path);
  p.drawPixmap(0, 0, profile_tmp);
  user_profile_[player] = profile_pixmap;
  return true;
}
