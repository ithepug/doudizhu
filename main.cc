#include "mainwindow.h"
#include "welcome.h"
#include "broadcast.h"
#include <QStringList>
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  Welcome w;
  Broadcast *broadcast = Broadcast::GetInstance();
  QObject::connect(broadcast,SIGNAL(UpdateListWidget(QStringList)),&w,SLOT(UpdateListWidget(QStringList)));
  w.show();

  return a.exec();
}
