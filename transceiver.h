#ifndef TRANSCEIVER_H
#define TRANSCEIVER_H

#include"localplayer.h"
#include"onlineplayer.h"

#include<QObject>
#include<QTcpServer>
#include<QTcpSocket>
#include<QHostInfo>
#include<QList>

class MainWindow;
class Doudizhu;

class Transceiver : public QObject
{
  Q_OBJECT

public:
  static Transceiver* GetInstance();
  void SetMode(const bool& mode);

  void ConnectToHost(const QString& hostName,const quint16& port);

  void TellServerLocalPlayerInfo(const Player& player);
  void TellServerIsBid(bool isbid);
  void HideClientBidButton();
  void TellClinetUpdateInfo(const LocalPlayer& server,const QList<OnlinePlayer*>& clinets);
  void DealCardToClient(int players, int card_id);
  void ClearAllClientCard();
  void TellClinetShowBidButton(OnlinePlayer* client);
  void TellClientUpdateCardsSize();
  void TellClientStopBid();
  void TellPlayerToPlayCard(const int &player);
  void TellClientPlayedCard(const QList<Card *> &played_card);
  void TellServerPlayedCard(const QList<Card *> &played_card);
  QList<Card*> ReadCard(QTcpSocket* socket);
  void WriteCard(const QList<Card *> &card, QTcpSocket* socket);
  void TellClientSetCurrentPlayer(const int &player_rank);
  void TellClientClearPlayingCard();
  void TellClientLoadMatchAndMatchScore();
  void TellClientLandLordRank(const int& rank);
  void TellAllClientProfile(const int& rank, const QByteArray& photo);
  void TellClientAllProfile(const int& rank, const QList<QByteArray> &photos);

private:
  explicit Transceiver(QObject* parent);

public slots:
  void Read();

private slots:
  // Server
  void OnNewConnectionToServer();
  void DisConnection();
  // Client
  void OnConnectedToServer();

signals:
  void YouNeedToStopBid();

public:
  const static bool MODE_SERVER;
  const static bool MODE_CLIENT;
  const static quint16 kPort_;

private:
  bool mode_;
  QTcpServer* tcpserver_ = nullptr;
  QTcpSocket* tcpclient_ = nullptr;
};

#endif // TRANSCEIVER_H
