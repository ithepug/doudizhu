#ifndef DOUDIZHU_MAINWINDOW_H_
#define DOUDIZHU_MAINWINDOW_H_

#include "button.h"
#include "localplayer.h"
#include "onlineplayer.h"
#include "playcard.h"
#include "transceiver.h"

#include <ctime>

#include <broadcast.h>
#include <QLabel>
#include <QMainWindow>
#include <QPushButton>
#include <QSound>
#include <QTcpServer>
#include <QTimer>

namespace Ui {
class MainWindow;
}

class Doudizhu;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  static MainWindow* GetInstance(Doudizhu* doudizhu, QString server_ip = "");
  ~MainWindow();
  void paintEvent(QPaintEvent*);
  void resizeEvent(QResizeEvent*);
  bool isBid();
  void SelectCard(Card* card_begin, int size);
  void DrawPlayingCard(int player);
  void HidePlayingCard(int player);
  void ClearAllPlayingCard();
  void UpdateScoreBaseLabel(const int &base);
  void ResizeCard2SizeL(Card &card);
  void ResizeCard2SizeS(Card &card);
  bool SetPlayerProfile(const int &player, const QByteArray& profile);

private:
  explicit MainWindow(Doudizhu* doudizhu, QWidget* parent = nullptr);
  explicit MainWindow(Doudizhu* doudizhu, QString server_ip, QWidget* parent = nullptr);
  void DrawPlayer(const Player& player);
  void Init();
  void InitLabel();
  void InitPushButton();
  void InitConnection();
  void ClearPlayerCard();
  static bool CardCmp(const Card* card1, const Card* card2);
  QPixmap RoundCorner(const QPixmap& src);
  bool AskPlayerforBid(const int& rank);
  void AddCardToPlay(Card* card);
  void DelCardToPlay(Card* card);
  void HideBidButton();
  void ClearLabel(const int &player);

signals:
  void YouNeedToDrawCard(int player);
  void CurrentPlayerPlayOut();

public slots:
  void DrawCard(const int& player);

private slots:
  void UpdateLabel();
  void ShowBidButton(int rank);
  void ShowPlayButton();
  void HidePlayButton();

public:

  int border_top_;
  int status_bar_top_;
  int primary_area_left_;
  int primary_area_top_;
  int primary_area_right_;
  int primary_area_bottom_;

  const QSize* kCardSizeL_;
  const QSize* kCardSizeS_;

  Doudizhu* doudizhu_;
  Transceiver* transceiver_;

private:
  Ui::MainWindow* ui;

  QPushButton* ready_;

  QPoint btn_centre_point_;

  QPixmap* card_back_right_left_;
  QPixmap* card_back_top_;
  QLabel* online_player_card_back_pic_[3];

  QLabel* label_local_player_name_;
  QLabel* label_local_player_money_;
  QLabel* label_score_base_;
  QLabel* label_online_player_name_[3];
  QLabel* label_online_player_money_[3];

  Button* btn_bid_;
  Button* btn_no_bid_;
  Button* btn_play_;
  Button* btn_pass_;
  Button* btn_prompt_;

  QPixmap* default_profile_;
  QPixmap* user_profile_[4];

  friend class Transceiver;
  friend class Doudizhu;
};

#endif  // DOUDIZHU_MAINWINDOW_H_
