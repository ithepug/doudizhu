#include "onlineplayer.h"

OnlinePlayer::OnlinePlayer():Player()
{
  cards_size_=0;
}

OnlinePlayer::OnlinePlayer(QUuid& uuid, QString name,
                           int money):
                                        Player(uuid, name, money)
{

}

OnlinePlayer::OnlinePlayer(QUuid &uuid, QString name, QByteArray photo, int money)
    :Player(name, name, photo, money)
{

}

void OnlinePlayer::set_tcpsocket(QTcpSocket* tcpsocket)
{
  tcpsocket_=tcpsocket;
}

QTcpSocket* OnlinePlayer::tcpsocket() const
{
  return tcpsocket_;
}

int OnlinePlayer::cards_size() const
{
  return cards_size_;
}

void OnlinePlayer::ZeroingCardsSize()
{
  cards_size_=0;
}

void OnlinePlayer::AddCardsSize(int len)
{
  cards_size_ += len;
}

void OnlinePlayer::SubCardsSize(int len)
{
  cards_size_ -= len;
  if(cards_size_<0)
    qDebug()<<"SubCardSize():cards_size < 0 !";
}

void OnlinePlayer::SetCardsSize(int size)
{
  cards_size_=size;
}

