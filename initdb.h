#ifndef INITDB_H
#define INITDB_H

#include <QtSql>

bool InitDB(QSqlDatabase& db) {
  db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("doudizhu.sqlite");

  // Skip create table if db existed
  db.setConnectOptions("QSQLITE_OPEN_READONLY");
  if (!db.open()) {
    // It may be the first time the program was opened
    // and the database does not exist.
    // Clear option, try again
    db.setConnectOptions();
    if (!db.open()) {
      qDebug() << "Open DB error:" + db.lastError().text();
      return false;
    }
    // Create table
    bool ret = true;
    QSqlQuery q;
    ret &= q.exec(
        "create table T_MATCH(PLAYER_ID_SUM blob not null,MATCH_ID integer not "
        "null constraint T_MATCH_pk primary key autoincrement);");
    ret &= q.exec(
        "create unique index T_MATCH_MATCH_RESULT_ID_uindex on T_MATCH "
        "(MATCH_ID);");
    ret &= q.exec(
        "create unique index T_MATCH_PLAYERIDSUM_uindex on T_MATCH "
        "(PLAYER_ID_SUM);");
    ret &= q.exec(
        "create table T_MATCH_RESULT(ID integer not null references "
        "T_MATCH,USER_ID blob,SCORE int,constraint T_MATCH_RESULT_pk primary "
        "key (ID, USER_ID));");
    ret &= q.exec(
        "create table T_USER_LOCAL( ID blob not null constraint user_pk "
        "primary key, NAME text, PHOTO blob);");
    ret &= q.exec(
        "create table T_USER_ONLINE( ID blob not null constraint "
        "T_USER_ONLINE_pk primary key, NAME text not null, PHOTO blob "
        ");");
    ret &= q.exec(
        "create unique index T_USER_ONLINE_ID_uindex on T_USER_ONLINE "
        "(ID);");
    return ret;
  }
  // Clear option
  db.setConnectOptions();
  db.open();
  return true;
}

#endif  // INITDB_H
