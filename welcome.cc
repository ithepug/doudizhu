#include "welcome.h"

Welcome::Welcome(QWidget* parent) : QMainWindow(parent), ui(new Ui::Welcome) {
  ui->setupUi(this);

  // Open DB by Recorder
  while (true) {
    recorder_ = Recorder::GetInstance();
    if (!recorder_->isReady()) {
      QMessageBox::critical(
          nullptr, "开启失败",
          "数据库初始化失败,未能成功开启游戏,请考虑更换游戏存放位置");
    } else {
      break;
    }
  }

  UpdateSelectRoleList();
}

Welcome::~Welcome() { delete ui; }

void Welcome::on_start_game_clicked() {
  QListWidgetItem* item = ui->listWidget->currentItem();
  if (item == nullptr) {
    QMessageBox::information(nullptr, "开始游戏失败", "未指定服务器！",
                             QMessageBox::Ok);
    return;
  }
  QString server_ip;
  int pos = item->text().indexOf('@');
  server_ip = item->text().mid(pos + 1);
  QByteArray uuid = getCurrentUuid();
  if (uuid.isEmpty()) {
    QMessageBox::information(nullptr, "开始游戏失败", "找不到对应用户的ID",
                             QMessageBox::Ok);
    return;
  }
  Doudizhu* doudizhu = Doudizhu::GetInstance(uuid, server_ip);  // CREATE!
  MainWindow* mainwindow =
      MainWindow::GetInstance(doudizhu, server_ip);  // CREATE!
  doudizhu->InitMainWindow(mainwindow);
  this->close();
}

void Welcome::on_create_server_clicked() {
  QByteArray uuid = getCurrentUuid();
  if (uuid.isEmpty()) {
    QMessageBox::information(nullptr, "开始游戏失败", "找不到对应用户的ID",
                             QMessageBox::Ok);
    return;
  }
  Doudizhu* doudizhu = Doudizhu::GetInstance(uuid);
  MainWindow* mainwindow = MainWindow::GetInstance(doudizhu);
  doudizhu->InitMainWindow(mainwindow);
  this->close();
}

void Welcome::UpdateListWidget(QStringList string_list) {
  int current_row;
  current_row = ui->listWidget->currentRow();
  ui->listWidget->clear();
  ui->listWidget->addItems(string_list);
  ui->listWidget->setCurrentRow(current_row);
}

void Welcome::on_listWidget_itemDoubleClicked(QListWidgetItem*) {
  on_start_game_clicked();
}

void Welcome::UpdateSelectRoleList() {
  QList<QString> combobox_list;
  for_select_role_ = recorder_->GetLocalPlayers();
  for (auto role : for_select_role_) {
    combobox_list.push_back(role.name_);
  }
  ui->selectRole->clear();
  ui->selectRole->addItems(combobox_list);
}

void Welcome::on_btn_add_role_clicked() {
  AddLocalPlayerwindow* add_player_window = new AddLocalPlayerwindow();
  connect(add_player_window, &AddLocalPlayerwindow::NewLocalPlayer, this,
          &Welcome::UpdateSelectRoleList);
  add_player_window->setAttribute(Qt::WA_DeleteOnClose);
  add_player_window->show();
}

QByteArray Welcome::getCurrentUuid() {
  QByteArray uuid;
  int index = ui->selectRole->currentIndex();
  if (index >= 0 && index < for_select_role_.size()) {
    uuid = for_select_role_[index].uuid_;
  } else {
    qDebug() << "getCurrentUuid():"
             << "index out of range";
  }
  return uuid;
}
