//This onlineplayer means the user's three opponents.

#ifndef DOUDIZHU_ONLINEPLAYER_H_
#define DOUDIZHU_ONLINEPLAYER_H_

#include"player.h"

#include<QTcpSocket>
#include<QPoint>

class OnlinePlayer:public Player
{
public:
  OnlinePlayer();
  OnlinePlayer(QUuid& uuid, QString name,int money = DEFAULT_SCORE);
  OnlinePlayer(QUuid& uuid, QString name,QByteArray photo, int money = DEFAULT_SCORE);
  void set_tcpsocket(QTcpSocket* tcpsocket);
  QTcpSocket* tcpsocket() const;
  int cards_size() const;
  void ZeroingCardsSize();
  void AddCardsSize(int len = 1);
  void SubCardsSize(int len = 1);
  void SetCardsSize(int size);

private:
  QTcpSocket* tcpsocket_;
  int cards_size_;
  bool isbid_;
};

#endif // DOUDIZHU_ONLINEPLAYER_H_
