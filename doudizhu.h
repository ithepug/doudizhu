﻿#ifndef DOUDIZHU_H
#define DOUDIZHU_H
#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>
#include <QtConcurrent/QtConcurrent>
#include <QMutex>

#include <card.h>
#include <localplayer.h>
#include <mainwindow.h>
#include <onlineplayer.h>
#include <playcard.h>
#include <player.h>
#include <transceiver.h>
#include <recorder.h>

class Doudizhu : public QObject {
  Q_OBJECT
private:
  Doudizhu(QByteArray localplayer_uuid, QString server_ip);

public:
  static Doudizhu* GetInstance(QByteArray localplayer_uuid = nullptr, QString server_ip = "");
  void InitMainWindow(MainWindow* mainwindow);

  void online_player_add(OnlinePlayer* player);
  int online_player_size() const;
  bool online_player_erase(QTcpSocket* socket);
  QTcpSocket* online_player_tcpsocket(const int& index) const;
  int online_player_card_size(const int& index) const;

  QByteArray local_player_photo() const;
  int local_player_card_size() const;

  QList<Card*> origin_card_list() const;
  int current_player() const;
  void TurnCurrentPlayer();
  void set_current_player(const int& player);
  void set_flag_player_bided(const bool& flag);
  void set_player_is_bid(const bool& is_bid);
  void set_landlord(Player* player);
  Player* blank_player() const;
  int score_base() const;
  void AddCardToPlay(Card* card);
  void DelCardToPlay(Card* card);
  static bool CardCmp(const Card* card1, const Card* card2);

  // Server
  void onReceivedPlayerInfoFromClient(const OnlinePlayer& player);                    // 0
  void onReceivedBidResultFromClient(const bool& isBid);                              // 5
  void onReceivedPlayedCardFromClient(const QList<Card*>& cards);                     // 10
  // Client
  void onReceivedPlayerInfoFromServer(const int& index, const OnlinePlayer& player);  // 1
  void onReceivedDealingCardFromServer(const int& card_index);                        // 2
  void onReceivedClearCardSignalFromServer();                                         // 3
  void onReceivedToBidSignalFromServer();                                             // 4
  void onReceivedPlayerCardSizeFromServer(const int& index, const int& size);         // 6
  void onReceivedStopBidSignalFromServer();                                           // 7
  void onReceivedToPlaySignalFromServer();                                            // 8
  void onReceivedPlayedCardFromServer(const QList<Card*>& cards);                     // 9
  void onReceivedCurrentPlayerFromServer(const int& index);                           // 11
  void onReceivedClearPlayingCardSignalFromServer();                                  // 12
  void onReceivedLoadMatchSignalFromServer();                                         // 14
  void onReceivedLandLordFromServer(const int& index);                                // 15
  void onReceivedPlayerPhotoFromServer(const int& index, const QByteArray& photo);    // 16
  void OnConnectedToServer();

  OnlinePlayer *FindPlayerBySocket(const QTcpSocket *player_socket);
  int FindOnlinePlayerRank(const OnlinePlayer* online_player);

private:
  void InitCard();
  void SetPlayer(Player* player, const QString& host_name);
  int GetRandomNumber(const int& max);
  void BidForLandlord();
  void StartGame();
  void Play(const QList<Card*>& played_card);
  void CountOut();
  void UpdatePlayingCard(QList<Card*> card);
  QList<Card*>* playing_card();
  QList<Card*>* card_to_play_list();
  bool LoadsMatch();
  bool LoadsPlayersMatchScore();

private slots:
  void StopBid();
  void ShuffleCard();
  void ShuffleOneCard();
  void btn_bid_clicked();
  void btn_no_bid_clicked();
  void btn_play_clicked();
  void btn_pass_clicked();
  void SetLandlord(const int& landlord_rank);

signals:
  void YouNeedToDrawCard(int player);
  void YouNeedToShuffleCard();
  void YouNeedToSetLandlord(int landlord_rank);
  void YouNeedToShowBidBtn(int rank);
  void YouNeedToUpdateLabel();

public:
  volatile bool flag_player_bided_;  // have players ever bid
  const QList<Card*> kNullCardList;
  QMutex bid_landlord_mutex_;

private:
  LocalPlayer* local_player_;
  QList<OnlinePlayer*> online_player_;
  Player* blank_player_;
  Player* landlord_;

  QList<Card*> origin_card_list_;
  QList<Card*> card_list_;
  QList<Card*> card_to_play_list_;
  QList<Card*> playing_card_[4];

  volatile bool player_is_bid_;  // do players bid
  volatile bool flag_stop_bid_for_landlord_;
  bool amiserver_;

  PlayCard* playcard_;

  int current_player_;  // [0,3] 0:self; 1-3:online player(clockwise)

  int score_base_;

  Recorder *recorder_;

  Transceiver* transceiver_;
  MainWindow* mainwindow_;

  QTimer* shuffle_interval_time;

  int match_id_;

  friend class MainWindow;
};

#endif  // DOUDIZHU_H
