#include "playcard.h"
#include "mainwindow.h"
#include <doudizhu.h>

PlayCard::PlayCard(QObject *parent) : QObject(parent)
{
  max_card_.type=0;
  max_card_.value=-1;
  max_card_.size=0;
}

bool PlayCard::Play(const QList<Card *> &card_to_play)
{
  return true;
  SimpleCard card=TypeJudge(card_to_play);
  if(card.type==-1)
  {
    qDebug()<<"Invalid card type";
    return false;//Invalid card type
  }
  if(max_card_owner_ == Doudizhu::GetInstance()->current_player())
  {
    return true;
  }
  if(max_card_.type!=0)//There are max card.
  {
    if(!CardCmp(max_card_,card))
    {
      qDebug()<<"Smaller than max card";
      return false;//Smaller than max card
    }
  }
  return true;
}

//Using TJAA(),TJAAA(),TJAAAA0()... to judge card_to_play card type.
//And trans card_to_play(QList) to SimpleCard.
//TJAA(),TJAAA()... these function will modify card(SimpleCard) and
//return true, if card typing successed.
//The end of function name , 0, means it can judge same type but
//different "size" card.(e.g. AABBCC and AABBCCDD).
SimpleCard PlayCard::TypeJudge(const QList<Card *> &card_to_play)
{
  SimpleCard card;
  card.type=-1;
  card.size=0;
  card.value=-1;
  int size=card_to_play.size();
  switch (size)
  {
  case 1:
    card.size=1;
    card.type=1;
    card.value=card_to_play[0]->card_point();
    break;
  case 2:
    TJAA(card_to_play,card);
    break;
  case 3:
    TJAAA(card_to_play,card);
    break;
  case 4:
    TJRRBB(card_to_play,card)||TJAAAA0(card_to_play,card);
    break;
  case 5:
    TJAAAA0(card_to_play,card)||TJAAACC(card_to_play,card)||TJABCDE0(card_to_play,card);
    break;
  case 6:
    TJAAAA0(card_to_play,card)||TJABCDE0(card_to_play,card)||TJAABBCC0(card_to_play,card)||TJAAABBB0(card_to_play,card);
    break;
  case 7:
    TJAAAA0(card_to_play,card)||TJABCDE0(card_to_play,card);
    break;
  case 8:
    TJAAAA0(card_to_play, card) || TJABCDE0(card_to_play, card) || TJAABBCC0(card_to_play, card);
    break;
  case 9:
    TJABCDE0(card_to_play,card)||TJAAABBB0(card_to_play,card);
    break;
  case 10:
    TJABCDE0(card_to_play,card)||TJAABBCC0(card_to_play,card)||TJAAACCBBBEE0(card_to_play,card);
    break;
  case 11:
    TJABCDE0(card_to_play,card);
    break;
  case 12:
    TJABCDE0(card_to_play,card)||TJAABBCC0(card_to_play,card)||TJAAABBB0(card_to_play,card);
    break;
  default:
    if(size>13)
    {
      if(size%2==0)
      {
        if(TJAABBCC0(card_to_play,card))
          break;
      }
      if(size%3==0)
      {
        if(TJAAABBB0(card_to_play,card))
          break;
      }
      if(size%5==0)
      {
        if(TJAAACCBBBEE0(card_to_play,card))
          break;
      }
    }
  }
  qDebug()<<"Judge Type :"<<card.type;
  return card;
}

bool PlayCard::TJAA(const QList<Card *> &card_to_play, SimpleCard &card)
{
  if(card_to_play[0]->card_point()==card_to_play[1]->card_point())
  {
    card.size=2;
    card.type=2;
    card.value=card_to_play[0]->card_point();
    return true;
  }
  else
  {
    return false;
  }
}

bool PlayCard::TJAAA(const QList<Card *> &card_to_play, SimpleCard &card)
{
  if(card_to_play[0]->card_point()==card_to_play[1]->card_point()
      &&card_to_play[1]->card_point()==card_to_play[2]->card_point())
  {
    card.size=3;
    card.type=3;
    card.value=card_to_play[0]->card_point();
    return true;
  }
  else
  {
    return false;
  }
}

bool PlayCard::TJAAACC(const QList<Card *> &card_to_play, SimpleCard &card)
{
  if(card_to_play.size()!=5)
    return false;
  if(card_to_play[0]->card_point()==card_to_play[1]->card_point())
  {
    if(card_to_play[1]->card_point()==card_to_play[2]->card_point())
    {
      if(card_to_play[3]->card_point()==card_to_play[4]->card_point())//AAACC
      {
        card.size=5;
        card.type=5;
        card.value=card_to_play[0]->card_point();
        return true;
      }
    }
    else if(card_to_play[2]->card_point()==card_to_play[3]->card_point()
             &&card_to_play[3]->card_point()==card_to_play[4]->card_point())//AACCC
    {
      card.size=5;
      card.type=5;
      card.value=card_to_play[2]->card_point();
      return true;
    }
  }
  return false;
}

bool PlayCard::TJABCDE0(const QList<Card *> &card_to_play, SimpleCard &card)
{
  //The max card more than card A(11).
  if(card_to_play[0]->card_point()>11)
    return false;
  int size=card_to_play.size()-1;
  for (int i=0;i!=size;i++)
  {
    if(card_to_play[i+1]->card_point()+1!=card_to_play[i]->card_point())
      return false;
  }
  card.size=size+1;
  card.type=6;
  card.value=card_to_play[0]->card_point();
  return true;
}

bool PlayCard::TJAAAA0(const QList<Card *> &card_to_play, SimpleCard &card)
{
  int size=card_to_play.size()-1;
  for (int i=0;i!=size;i++)
  {
    if(card_to_play[i]->card_point()!=card_to_play[i+1]->card_point())
      return false;
  }
  card.size=size+1;
  card.type=4;
  card.value=card_to_play[0]->card_point();
  return true;
}

bool PlayCard::TJRRBB(const QList<Card *> &card_to_play, SimpleCard &card)
{
  if(card_to_play[0]->card_id()==104)//First of all, judge rocket.
  {
    card.size=4;
    card.type=10;
    card.value=104;
    return true;
  }
  else
  {
    return false;
  }
}

bool PlayCard::TJAABBCC0(const QList<Card *> &card_to_play, SimpleCard &card)
{
  //The max card more than card A(11).
  if (card_to_play[0]->card_point() > 11)
    return false;
  int size=card_to_play.size();
  if(size%2 != 0)
    return false;
  for(int i=0;i!=size;i+=2)
  {
    if(card_to_play[i]->card_point()!=card_to_play[i+1]->card_point())
      return false;
  }
  for(int i=2;i!=size;i+=2)
  {
    if(card_to_play[i]->card_point()+1!=card_to_play[i-1]->card_point())
      return false;
  }
  card.size=size;
  card.type=7;
  card.value=card_to_play[0]->card_point();
  return true;
}

bool PlayCard::TJAAABBB0(const QList<Card *> &card_to_play, SimpleCard &card)
{
  //The max card more than card A(11).
  if (card_to_play[0]->card_point() > 11)
    return false;
  int size=card_to_play.size();
  if(size%3 != 0)
    return false;
  for(int i=0;i!=size;i+=3)
  {
    if(card_to_play[i]->card_point()!=card_to_play[i+1]->card_point())
      return false;
    if(card_to_play[i+1]->card_point()!=card_to_play[i+2]->card_point())
      return false;
  }
  for(int i=3;i!=size;i+=3)
  {
    if(card_to_play[i]->card_point()+1!=card_to_play[i-1]->card_point())
      return false;
  }
  card.size=size;
  card.type=8;
  card.value=card_to_play[0]->card_point();
  return true;
}

bool PlayCard::TJAAACCBBBEE0(const QList<Card *> &card_to_play, SimpleCard &card)
{
  int c=0;
  int c2=0;
  int c3=0;
  int p3[7];
  int size = card_to_play.size()-1;
  if((size+1)%5 != 0)
    return false;
  for(int i=0;i!=size;i++,c++)
  {
    if(card_to_play[i]->card_point()!=card_to_play[i+1]->card_point())
    {
      if(c==0)
      {
        return false;
      }
      else if(c==1)
      {
        c=-1;
        c2++;
      }
      else if(c==2)
      {
        c=-1;
        p3[c3]=card_to_play[i]->card_point();
        c3++;
      }
    }
    else if (i == size - 1)
    {
      if (c == 0)
      {
        c2++;
      }
      else if (c == 1)
      {
        p3[c3] = card_to_play[i]->card_point();
        c3++;
      }
    }
  }
  if(c2==c3)
  {
    c3--;
    for(int i=0;i!=c3;i++)
      if(p3[i+1]+1!=p3[i])
        return false;
    //The max card more than card A(11).
    if (card_to_play[0]->card_point() > 11)
      return false;
    card.size=size+1;
    card.type=9;
    card.value=p3[0];
    return true;
  }
  return false;
}


bool PlayCard::CardCmp(const SimpleCard &card1, const SimpleCard &card2)
{
  if(card2.type==10)//RRBB
  {
    return true;
  }
  else if(card2.type==4)
  {
    if(card1.type==10)
      return false;
    if(card1.type==4)
    {
      if(card2.size<card1.size)
        return false;
      else if(card2.size>card1.size)
        return true;
      else if(card2.value<=card1.value)   //size is equals
        return false;
    }
    return true;
  }
  else if(card1.type!=card2.type)
  {
    return false;
  }
  else if(card1.size!=card2.size)
  {
    return false;
  }
  else if(card1.value>=card2.value)
  {
    return false;
  }
  return true;
}

void PlayCard::SetCurrentMaxCard(int owner, const QList<Card *> &card)
{
  SimpleCard simple_card=TypeJudge(card);
  //convert to simple card for card compare convenient.

  if(simple_card.type == -1)
    qDebug()<<"SetCurrentMaxCard(): invaild card type!";

  max_card_ = simple_card;
  max_card_owner_ = owner;
}

void PlayCard::ClearCurrentMaxCard()
{
  max_card_.type = 0;
}

int PlayCard::max_card_owner() const
{
  return max_card_owner_;
}
