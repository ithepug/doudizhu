#ifndef DOUDIZHU_PLAYER_H_
#define DOUDIZHU_PLAYER_H_

#define DEFAULT_SCORE 1000

#include<QObject>
#include<QString>
#include<QUuid>
#include<QPixmap>

class Player : public QObject
{
  Q_OBJECT
public:
  Player();
  Player(const QUuid& uuid, const QString& name,
         const int& money = DEFAULT_SCORE);
  Player(const QUuid& uuid, const QString& name, const QByteArray& photo,
         const int& money = DEFAULT_SCORE);
  QUuid uuid() const;
  QUuid set_uuid(QUuid uuid);
  QString name() const;
  void set_name(QString name);
  int money() const;
  void set_money(int money);
  QByteArray photo() const;
  void set_photo(QByteArray photo);
  virtual int cards_size() const;

private:
  QUuid uuid_;
  QString name_;
  QByteArray photo_;
  int money_;
};

#endif // DOUDIZHU_PLAYER_H_
