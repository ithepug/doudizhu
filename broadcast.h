#ifndef BROADCAST_H
#define BROADCAST_H

#include <QObject>
#include <QUdpSocket>
#include <QHostAddress>
#include <QNetworkAddressEntry>
#include <QCborStreamReader>
#include <set>

struct SeverHostPort{
  QString name_;
  QHostAddress address_;
  quint16 port_;

  bool operator<(const SeverHostPort &other) const
  {
    return this->port_ < other.port_;
  }
};

class Broadcast : public QObject
{
  Q_OBJECT
private:
  explicit Broadcast(QObject *parent = nullptr);

public:
  static Broadcast* GetInstance();
  void SetServerPort(const quint16 &port);
  void SetServerName(const QString &name);

signals:
  void UpdateListWidget(QStringList server_list);

private:
  void UpdateAddresses();
  void UpdateServerList();

public slots:
  void SendBroadcastDatagram();
  void ReadBroadcastDatagram();

private:
  QUdpSocket broadcast_socket_;
  QList<QHostAddress> broadcast_addresses_;
  std::set<SeverHostPort> server_set_;
  quint16 server_port_;
  QString username_;
};

#endif // BROADCAST_H
