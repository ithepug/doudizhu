#include "recorder.h"
#include "initdb.h"

Recorder *Recorder::GetInstance() {
  static Recorder *unique_instance = new Recorder();
  return unique_instance;
}

Recorder::Recorder(QObject *parent) : QObject(parent) {
  is_ready_ = false;
  if (InitDB(db_)) {
    is_ready_ = true;
  }
}

bool Recorder::isReady() { return is_ready_; }

bool Recorder::AddLocalPlayer(const LocalPlayer &player) {
  QSqlQuery q;
  q.prepare(
      "insert into T_USER_LOCAL (ID, NAME, PHOTO)"
      "values(:id, :name, :photo)");
  q.bindValue(":id", player.uuid().toRfc4122());
  q.bindValue(":name", player.name());
  q.bindValue(":photo", player.photo());
  if (!q.exec()) {
    qDebug() << q.lastError();
    return false;
  }
  return true;
}

bool Recorder::AddOnlinePlayer(const OnlinePlayer &player) {
  QSqlQuery q;
  q.prepare(
      "insert into T_USER_ONLINE (ID, NAME)"
      "values(:id, :name)");
  q.bindValue(":id", player.uuid().toRfc4122());
  q.bindValue(":name", player.name());
  if (!q.exec()) {
    if (QString::compare(q.lastError().driverText(),
                         "UNIQUE constraint failed: T_USER_ONLINE.ID")) {
      return true;
    }
    qDebug() << q.lastError();
    return false;
  }
  return true;
}

LocalPlayer *Recorder::GetLocalPlayerByUuid(const QByteArray &uuid_byte) {
  LocalPlayer *player = nullptr;
  QUuid uuid = QUuid::fromRfc4122(uuid_byte);
  QSqlQuery q;
  QString sql =
      "select * from T_USER_LOCAL where ID= x'" + uuid_byte.toHex() + "'";
  if (!q.exec(sql)) {
    qDebug() << q.lastError().text();
  }
  if (q.next()) {
    QString name = q.value(1).toString();
    QByteArray photo = q.value(2).toByteArray();
    player = new LocalPlayer(uuid, name, photo);
  }
  return player;
}

QList<EasyPlayer> Recorder::GetLocalPlayers() {
  QList<EasyPlayer> ret;

  QSqlQuery q;
  if (!q.exec("select ID,NAME from T_USER_LOCAL")) {
    qDebug() << q.lastError().text();
  }
  while (q.next()) {
    QByteArray uuid = q.value(0).toByteArray();
    QString name = q.value(1).toString();
    EasyPlayer easy_player(uuid, name);
    ret.push_back(easy_player);
  }
  return ret;
}

int Recorder::GetMatchIDByMatchUUID(const QByteArray &uuid) {
  int ret = -1;
  QSqlQuery q;
  // Try to select
  QString sql =
      "select * from T_MATCH where PLAYER_ID_SUM= x'" + uuid.toHex() + "'";
  if (!q.exec(sql)) {
    qDebug() << q.lastError().text();
  }
  if (q.next()) {
    int index = q.record().indexOf("MATCH_ID");
    ret = q.value(index).toInt();
  } else {
    // Not found, Insert
    q.prepare(
        "insert into T_MATCH (PLAYER_ID_SUM)"
        "values(:id_sum)");
    q.bindValue(":id_sum", uuid);
    if (!q.exec()) {
      qDebug() << q.lastError();
    } else {
      ret = q.lastInsertId().toInt();
    }
  }
  return ret;
}

int Recorder::GetPlayerMatchScore(const int &match_id,
                                  const QByteArray &player_id) {
  int ret = -1;
  QSqlQuery q;
  // Try to select
  QString sql = "select * from T_MATCH_RESULT where ID= '" +
                QString::number(match_id) + "' and USER_ID=x'" +
                player_id.toHex() + "'";
  if (!q.exec(sql)) {
    qDebug() << q.lastError().text();
  }
  if (q.next()) {
    int index = q.record().indexOf("SCORE");
    ret = q.value(index).toInt();
  } else {
    // Not found, Insert
    q.prepare(
        "insert into T_MATCH_RESULT (ID, USER_ID, SCORE)"
        "values(:id, :user_id, :score)");
    q.bindValue(":id", match_id);
    q.bindValue(":user_id", player_id);
    q.bindValue(":score", DEFAULT_SCORE);
    if (!q.exec()) {
      qDebug() << q.lastError();
    } else {
      ret = DEFAULT_SCORE;
    }
  }
  return ret;
}

bool Recorder::UpdateMatchResult(const int &match_id,
                                 const QByteArray &player_id,
                                 const int &player_score) {
  QSqlQuery q;
  QString sql =
      "update T_MATCH_RESULT set SCORE = :score where ID = :match_id and "
      "USER_ID = :user_id";
  q.prepare(sql);
  q.bindValue(":score", player_score);
  q.bindValue(":match_id", match_id);
  q.bindValue(":user_id", player_id);
  if (!q.exec()) {
    qDebug() << q.lastError();
    return false;
  }
  return true;
}
