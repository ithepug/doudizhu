#ifndef ADDLOCALPLAYERWINDOW_H
#define ADDLOCALPLAYERWINDOW_H

#include <QFileDialog>
#include <QUuid>
#include <QWidget>
#include "localplayer.h"
#include "recorder.h"

namespace Ui {
class AddLocalPlayerwindow;
}

class AddLocalPlayerwindow : public QWidget {
  Q_OBJECT

public:
  explicit AddLocalPlayerwindow(QWidget *parent = nullptr);
  ~AddLocalPlayerwindow();

signals:
  void NewLocalPlayer();

private slots:
  void on_btn_cancel_clicked();

  void on_btn_confirm_clicked();

  void on_btn_pick_profile_clicked();

private:
  QPixmap *picked_profile_;
  QByteArray picked_profile_byte_;
  Ui::AddLocalPlayerwindow *ui;
};

#endif  // ADDLOCALPLAYERWINDOW_H
