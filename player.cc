#include "player.h"

Player::Player()
{
  money_ = DEFAULT_SCORE;
}

Player::Player(const QUuid& uuid, const QString& name,
               const int& money)
{
  uuid_ = uuid;
  name_=name;
  money_=money;
}

Player::Player(const QUuid &uuid, const QString &name, const QByteArray &photo, const int &money)
{
  uuid_ = uuid;
  name_=name;
  money_=money;
  QPixmap pixmap;
  if(pixmap.loadFromData(photo))
  {
    photo_ = photo;
  }
}

QUuid Player::uuid() const
{
  return uuid_;
}

QUuid Player::set_uuid(QUuid uuid)
{
  uuid_ = uuid;
}

QString Player::name() const
{
  return name_;
}

void Player::set_name(QString name)
{
  name_=name;
}

int Player::money() const
{
  return money_;
}

void Player::set_money(int money)
{
  money_=money;
}

QByteArray Player::photo() const
{
  return photo_;
}

void Player::set_photo(QByteArray photo)
{
  photo_ = photo;
}

int Player::cards_size() const
{
  return -1;
}

