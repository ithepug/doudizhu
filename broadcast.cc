#include "broadcast.h"

static const quint16 kBroadcast_port = 45000;

Broadcast::Broadcast(QObject *parent) : QObject(parent)
{
  broadcast_socket_.bind(QHostAddress::AnyIPv4, kBroadcast_port,
                         QUdpSocket::ShareAddress
                             | QUdpSocket::ReuseAddressHint);
  connect(&broadcast_socket_, SIGNAL(readyRead()),this,
          SLOT(ReadBroadcastDatagram()));
  UpdateAddresses();
}

Broadcast *Broadcast::GetInstance()
{
  static Broadcast* unique_instance = new Broadcast();
  return unique_instance;
}

void Broadcast::SetServerPort(const quint16 &port)
{
  server_port_ = port;
}

void Broadcast::SetServerName(const QString &name)
{
  username_ = name;
}

void Broadcast::SendBroadcastDatagram()
{
  QByteArray datagram;
  {
    QCborStreamWriter writer(&datagram);
    writer.startArray(2);
    writer.append(username_);
    writer.append(server_port_);
    writer.endArray();
  }
  bool need_update_address = false;
  for (const auto &address:qAsConst(broadcast_addresses_))
  {
    if(broadcast_socket_.writeDatagram(datagram,
                                        address,
                                        kBroadcast_port) == -1)
      need_update_address = true;
  }
  if(need_update_address)
    UpdateAddresses();
}

void Broadcast::ReadBroadcastDatagram()
{
  while (broadcast_socket_.hasPendingDatagrams())
  {
    QHostAddress sender_ip;
    quint16 sender_port;
    QByteArray datagram;
    datagram.resize(static_cast<int>(broadcast_socket_.pendingDatagramSize()));
    if (broadcast_socket_.readDatagram(datagram.data(), datagram.size(),
                                       &sender_ip, &sender_port) == -1)
      continue;
    QString server_name;
    quint16 server_port;
    {
      // decode the datagram
      QCborStreamReader reader(datagram);
      if (reader.lastError() != QCborError::NoError || !reader.isArray())
        continue;
      if (!reader.isLengthKnown() || reader.length() != 2)
        continue;

      reader.enterContainer();

      if (reader.lastError() != QCborError::NoError || !reader.isString())
        continue;
      server_name = reader.readString().data;
      reader.readString();

      if (reader.lastError() != QCborError::NoError || !reader.isUnsignedInteger())
        continue;
      server_port = static_cast<quint16>(reader.toInteger());

      server_set_.insert({server_name,sender_ip,server_port});
      UpdateServerList();
    }
  }
}

void Broadcast::UpdateAddresses()
{
  broadcast_addresses_.clear();
  const QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces();
  for (const QNetworkInterface &interface : interfaces)
  {
    const QList<QNetworkAddressEntry> entries = interface.addressEntries();
    for (const QNetworkAddressEntry &entry : qAsConst(entries))
    {
      QHostAddress broadcastAddress = entry.broadcast();
      if (broadcastAddress != QHostAddress::Null && entry.ip() != QHostAddress::LocalHost)
      {
        broadcast_addresses_ << broadcastAddress;
      }
    }
  }
}

void Broadcast::UpdateServerList()
{
  QStringList servers;
  for(const auto &server:qAsConst(server_set_))
    servers << server.name_ + "@" + server.address_.toString();
  emit UpdateListWidget(servers);
}
