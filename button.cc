#include "button.h"

const QSize* const Button::kSize_=new QSize (115,45);

Button::Button(QWidget *parent) : QPushButton(parent)
{
  this->setCursor(Qt::PointingHandCursor);
  this->resize(*kSize_);
  this->hide();
}

void Button::SetImage(QString imageurl)
{
  this->setStyleSheet("border-image: url("+imageurl+')');
}
