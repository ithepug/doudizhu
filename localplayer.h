//This localplayer means user's player.
//Differ from onlineplayer, localplayer have card_(QList)
//that is used to represent user-owned card.

#ifndef DOUDIZHU_LOCALPLAYER_H_
#define DOUDIZHU_LOCALPLAYER_H_

#include"player.h"
#include"card.h"

#include<QList>


class LocalPlayer : public Player
{
  Q_OBJECT
public:
  LocalPlayer();
  LocalPlayer(QUuid& uuid, QString name,int money = DEFAULT_SCORE);
  LocalPlayer(QUuid& uuid, QString name, QByteArray photo, int money = DEFAULT_SCORE);

signals:
  void YouNeedToDrawCard(const int &player);

public:
  void AddCard(Card* card);
  void ClearCard();
  QList<Card*>* cards();
  int cards_size() const;

private:
  QList<Card*> cards_;
};

#endif // DOUDIZHU_LOCALPLAYER_H_
